# Visual Formula Structure

This project contains code for representing and performing operations on the visual structure of formulas.

## Build

```sh
npm run build
```

## Build + Watch

```sh
npm run start
```
