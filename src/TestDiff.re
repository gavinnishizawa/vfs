
let testDiff(a, b, target) = TestUtil.test(
  () => {
    let d = Diff.diff(a, b);
    // d |> Diff.toString(fun | None => "" | Some(v) => v) |> Js.log;
    d;
  },
  target,
);

let testDiff0() = testDiff(
  Node.make("a", "a"),
  Node.make("a", "a"),
  {
    ...Diff.empty,
    eq: [Node.make("a", "a")],
  }
);

let testDiff1() = testDiff(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
  |> Node.relate(Relations.SUPER, Node.make("c", "c")),
  Node.make("x", "x")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  ),
  {
    eq: [{
      ...Node.make("b", "b"),
      rPath: [Relations.RIGHT],
    }],
    minus: [
      {
        ...Node.make("c", "c"),
        rPath: [Relations.SUPER],
      }
    ],
    plus: [
      {
        ...Node.make("c", "c"),
        rPath: [Relations.SUPER, Relations.RIGHT],
      }
    ],
    delta: [
      (
        Node.make("a", "a"),
        Node.make("x", "x"),
      ),
    ],
  }
);

let testDiff2() = testDiff(
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.makeTyped("s", Node.ROOT, "")
      |> Node.relate(Relations.CONTAINS, Node.make("a", "a"))
      |> Node.relate(Relations.SUPER, Node.make("b", "b"))
    )
  ),
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.makeTyped("s", Node.ROOT, "")
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
      |> Node.relate(Relations.SUB, Node.make("d", "d")
        |> Node.relate(Relations.RIGHT, Node.make("e", "e")
          |> Node.relate(Relations.RIGHT, Node.make("f", "f"))
        )
      )
    )
  ),
  {
    eq: [
      Node.make("y", "y"),
      {
        ...Node.make("=", "="),
        rPath: [Relations.RIGHT],
      },
      {
        ...Node.makeTyped("s", Node.ROOT, ""),
        rPath: [Relations.RIGHT, Relations.RIGHT],
      },
    ],
    minus: [
      {
        ...Node.make("a", "a"),
        rPath: [Relations.CONTAINS, Relations.RIGHT, Relations.RIGHT],
      },
    ],
    plus: [
      {
        ...Node.make("d", "d"),
        rPath: [Relations.SUB, Relations.RIGHT, Relations.RIGHT],
      },
      {
        ...Node.make("e", "e"),
        rPath: [Relations.RIGHT,
          Relations.SUB, Relations.RIGHT, Relations.RIGHT],
      },
      {
        ...Node.make("f", "f"),
        rPath: [Relations.RIGHT, Relations.RIGHT,
          Relations.SUB, Relations.RIGHT, Relations.RIGHT],
      },
    ],
    delta: [
      (
        {
          ...Node.make("b", "b"),
          rPath: [Relations.SUPER, Relations.RIGHT, Relations.RIGHT],
        },
        {
          ...Node.make("c", "c"),
          rPath: [Relations.SUPER, Relations.RIGHT, Relations.RIGHT],
        },
      ),
    ],
  }
);

let runDiff() = {
  [
    testDiff0,
    testDiff1,
    testDiff2,
  ]
  |> TestUtil.runTests("Diff");
};


let run() = {
  runDiff();
};