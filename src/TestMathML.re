
let testCreate(mathmlObj, target) = TestUtil.test(
  () => mathmlObj -> MathML.buildTree,
  target,
);

let makeMathMLObj = MathML.makeObj;

let testCreate0() = testCreate(
  {
    attributes: {
      mathcolor: "a"
    },
    tagName: "math",
    value: Some("a"),
    children: [||]
  },
  Node.make("a", "a"),
);

let testCreate1() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("a", "mi", "a", [||]),
    makeMathMLObj("b", "mi", "b", [||]),
  |]),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
);

let testCreate2() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("..", "msup", "", [|
      makeMathMLObj("a", "mi", "a", [||]),
      makeMathMLObj("b", "mi", "b", [||]),
    |]),
  |]),
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b"))
);

let testCreate3() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("a", "mi", "a", [||]),
    makeMathMLObj("b", "mi", "b", [||]),
    makeMathMLObj("c", "mi", "c", [||]),
  |]),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c"))
  )
);

let testCreate4() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("y", "mi", "y", [||]),
    makeMathMLObj("=", "mo", "=", [||]),
    makeMathMLObj("...", "msupsub", "", [|
      makeMathMLObj("a", "mi", "a", [||]),
      makeMathMLObj("b", "mi", "b", [||]),
      makeMathMLObj("c", "mi", "c", [||]),
    |]),
  |]),
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a")
      |> Node.relate(Relations.SUPER, Node.make("b", "b"))
      |> Node.relate(Relations.SUB, Node.make("c", "c"))
    )
  )
);

let testCreate5() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("y", "mi", "y", [||]),
    makeMathMLObj("=", "mo", "=", [||]),
    makeMathMLObj("..", "mover", "", [|
      makeMathMLObj("a", "mi", "a", [||]),
      makeMathMLObj("^", "mi", "^", [||]),
    |]),
  |]),
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a")
      |> Node.relate(Relations.ABOVE, Node.make("^", "^"))
    )
  )
);

let testCreate6() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("f", "mfrac", "f", [|
      makeMathMLObj("a", "mi", "a", [||]),
      makeMathMLObj("b", "mi", "b", [||]),
    |]),
  |]),
  Node.makeTyped("f", Node.FRAC, "")
  |> Node.relate(Relations.ABOVE, Node.make("a", "a"))
  |> Node.relate(Relations.BELOW, Node.make("b", "b"))
);

let testCreate7() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("s", "msqrt", "s", [|
      makeMathMLObj("a", "mi", "a", [||]),
    |]),
  |]),
  Node.makeTyped("s", Node.ROOT, "")
  |> Node.relate(Relations.CONTAINS, Node.make("a", "a"))
);

let testCreate8() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("s", "mroot", "s", [|
      makeMathMLObj("a", "mi", "a", [||]),
      makeMathMLObj("b", "mi", "b", [||]),
    |]),
  |]),
  Node.makeTyped("s", Node.ROOT, "")
  |> Node.relate(Relations.CONTAINS, Node.make("a", "a"))
  |> Node.relate(Relations.PRESUPER, Node.make("b", "b"))
);

let testCreate9() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("r", "mtr", "", [|
      makeMathMLObj("c1", "mtd", "", [|
        makeMathMLObj("a", "mi", "a", [||]),
      |]),
      makeMathMLObj("c2", "mtd", "", [|
        makeMathMLObj("b", "mi", "b", [||]),
      |]),
    |]),
  |]),
  Node.make("a", "a")
  |> Node.relate(Relations.COL, Node.make("b", "b"))
);

let testCreate10() = testCreate(
  makeMathMLObj(".", "math", "", [|
    makeMathMLObj("t", "mtable", "", [|
      makeMathMLObj("r1", "mtr", "", [|
        makeMathMLObj("c1", "mtd", "", [|
          makeMathMLObj("a", "mi", "a", [||]),
        |]),
        makeMathMLObj("c2", "mtd", "", [|
          makeMathMLObj("b", "mi", "b", [||]),
        |]),
      |]),
      makeMathMLObj("r2", "mtr", "", [|
        makeMathMLObj("c3", "mtd", "", [|
          makeMathMLObj("c", "mi", "c", [||]),
        |]),
        makeMathMLObj("c4", "mtd", "", [|
          makeMathMLObj("d", "mi", "d", [||]),
        |]),
      |]),
    |]),
  |]),
  Node.makeTyped("t", Node.TABLE, "")
  |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
    |> Node.relate(Relations.COL, Node.make("b", "b"))
    |> Node.relate(Relations.ROW, Node.make("c", "c")
      |> Node.relate(Relations.COL, Node.make("d", "d")))
    )
);

let runCreate() = {
  [
    testCreate0,
    testCreate1,
    testCreate2,
    testCreate3,
    testCreate4,
    testCreate5,
    testCreate6,
    testCreate7,
    testCreate8,
    testCreate9,
    testCreate10,
  ]
  |> TestUtil.runTests("MathML Create");
};

let run() = {
  runCreate();
};