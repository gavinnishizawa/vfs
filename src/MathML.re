  type node('a) = Node.t('a, unit);

  type mathmlObjAttr = {
    mathcolor: string,
  };
  type mathmlObj = {
    attributes: mathmlObjAttr,
    tagName: string,
    value: option(string),
    children: array(mathmlObj),
  };

  let makeObj(id, tag, value, children) : mathmlObj = {
    attributes: {
      mathcolor: id,
    },
    tagName: tag,
    value: Some(value),
    children: children
  };


  let optStr(s_opt) = switch (s_opt) {
    | None => ""
    | Some(s) => s
  };
  // type mathmlNodeValue = {
  //   _type: string,
  //   value: string,
  //   color: string,
  // };


  let mmlId(mmlObj) = mmlObj.attributes.mathcolor;
  let mmlValue(mmlObj) = mmlObj.value->optStr->String.trim;

  let rec
  makeRow(mmlChildren) = switch (mmlChildren |> Array.to_list |> List.rev) {
    | [] => None;
    | [hd, ...tl] => tl
      |> List.map(buildTree)
      |> List.fold_left((next, cur) => cur
        |> Node.relate(Relations.RIGHT, next),
        hd->buildTree)
      |> st => Some(st);
  }

  and makeSuper(a, b) = a->buildTree
    |> Node.relate(Relations.SUPER, b->buildTree)

  and makeSub(a, b) = a->buildTree
    |> Node.relate(Relations.SUB, b->buildTree)

  and makeSuperSub(a, b, c) = a->buildTree
    |> Node.relate(Relations.SUPER, b->buildTree)
    |> Node.relate(Relations.SUB, c->buildTree)

  and makeOver(a, b) = {
    // <mover>a b</mover>
    // a -above-> \\overset -contains-> b
    // \\overset{b}{a}
    a->buildTree
    |> Node.relate(Relations.ABOVE, b->buildTree);
  }

  and makeUnder(a, b) = {
    // <munder>a b</under>
    // a -above-> \\underset -contains-> b
    // \\underset{b}{a}
    a->buildTree
    |> Node.relate(Relations.BELOW, b->buildTree);
  }
  and makeUnderOver(a, b, c) = {
    a->buildTree
    |> Node.relate(Relations.BELOW, b->buildTree)
    |> Node.relate(Relations.ABOVE, c->buildTree)
  }

  and makeFrac(id, a_opt, b_opt) = {
    Node.makeTyped(id, Node.FRAC, "")
    |> n => switch(a_opt) {
      | None => n
      | Some(a) => n
        |> Node.relate(Relations.ABOVE, a->buildTree)
    }
    |> n => switch(b_opt) {
      | None => n
      | Some(b) => n
      |> Node.relate(Relations.BELOW, b->buildTree)
    }
  }

  and makeRoot(id, a_opt, b_opt) = {
    Node.makeTyped(id, Node.ROOT, "")
    |> n => switch(a_opt) {
      | None => n
      | Some(a) => n
        |> Node.relate(Relations.CONTAINS, a->buildTree)
    }
    |> n => switch(b_opt) {
      | None => n
      | Some(b) => n
        |> Node.relate(Relations.PRESUPER, b->buildTree)
    }
  }

  and makeTable(id, a_opt) = {
    Node.makeTyped(id, Node.TABLE, "")
    |> n => switch(a_opt) {
      | None => n
      | Some(a) => n
        |> Node.relate(Relations.CONTAINS, a)
    }
  }
  and makeTableRow(mmlChildren) = switch (mmlChildren |> Array.to_list |> List.rev) {
    | [] => None;
    | [hd, ...tl] => tl
      |> List.map(buildTree)
      |> List.fold_left((next, cur) => cur
        |> Node.relate(Relations.ROW, next),
        hd->buildTree)
      |> st => Some(st);
  }
  and makeTableCol(mmlChildren) = switch (mmlChildren |> Array.to_list |> List.rev) {
    | [] => None;
    | [hd, ...tl] => tl
      |> List.map(buildTree)
      |> List.fold_left((next, cur) => cur
        |> Node.relate(Relations.COL, next),
        hd->buildTree)
      |> st => Some(st);
  }

  and buildTree(mmlObj : mathmlObj) = {
    let id = mmlObj.attributes.mathcolor;
    let node = id-> Node.make(mmlObj->mmlValue);

    switch(mmlObj.tagName) {
      | "math" => switch(mmlObj.children -> makeRow) {
        | None => node
        | Some(st) => st
      }
      | "mrow" => switch(mmlObj.children -> makeRow) {
        | None => node
        | Some(st) => if (st->Node.size > 1) {
          // Don't make container nodes for now
          // node |> Node.relate(Relations.CONTAINS, st);
          st
        } else st
      }
      | "msup" => switch(mmlObj.children -> Array.to_list) {
        | [hd] => hd->buildTree
        | [a, b, ..._] => makeSuper(a, b)
        | _ => node
      }
      | "msub" => switch(mmlObj.children -> Array.to_list) {
        | [hd] => hd->buildTree
        | [a, b, ..._] => makeSub(a, b)
        | _ => node
      }
      | "msupsub" => switch(mmlObj.children -> Array.to_list) {
        | [hd] => hd->buildTree
        | [a, b] => makeSuper(a, b)
        | [a, b, c, ..._] => makeSuperSub(a, b, c)
        | _ => node
      }
      | "msubsup" => switch(mmlObj.children -> Array.to_list) {
        | [hd] => hd->buildTree
        | [a, b, c, ..._] => makeSuperSub(a, c, b)
        | _ => node
      }
      | "mover" => switch(mmlObj.children -> Array.to_list) {
        | [hd] => hd->buildTree
        | [a, b, ..._] => makeOver(a, b)
        | _ => node
      }
      | "munder" => switch(mmlObj.children -> Array.to_list) {
        | [hd] => hd->buildTree
        | [a, b, ..._] => makeUnder(a, b)
        | _ => node
      }
      | "munderover" => switch(mmlObj.children -> Array.to_list) {
        | [hd] => hd->buildTree
        | [a, b, c, ..._] => makeUnderOver(a, b, c)
        | _ => node
      }
      | "mfrac" => switch(mmlObj.children -> Array.to_list) {
        | [] => makeFrac(id, None, None)
        | [a] => makeFrac(id, Some(a), None)
        | [a, b, ..._] => makeFrac(id, Some(a), Some(b))
      }
      | "msqrt" | "mroot" => switch(mmlObj.children -> Array.to_list) {
        | [] => makeRoot(id, None, None)
        | [a] => makeRoot(id, Some(a), None)
        | [a, b, ..._] => makeRoot(id, Some(a), Some(b))
      }
      | "mtable" => switch(mmlObj.children -> makeTableRow) {
        | None => makeTable(id, None)
        | Some(a) => makeTable(id, Some(a))
      }
      | "mtr" => switch(mmlObj.children -> makeTableCol) {
        | None => node
        | Some(st) => st
      }
      | "mtd" => switch(mmlObj.children -> makeRow) {
        | None => node
        | Some(st) => st
      }
      | _ => node;
    };
  };

  let rec toMathMLObject(node: node(string)) = {
    let idStr = switch (node.id) {
    | None => ""
    | Some(s) => s
    };
    let valueStr = switch (node.value) {
    | None => ""
    | Some(s) => s
    };

    switch (node.nodeType) {
    | Node.FRAC => makeObj(idStr, "mfrac", valueStr, [|
      switch(node->Node.getRelation(Relations.ABOVE)) {
        | None => makeObj("", "mrow", "", [||])
        | Some(above) => makeObj("", "mrow", "", [|
          above->toMathMLObject,
        |])
      },
      switch(node->Node.getRelation(Relations.BELOW)) {
        | None => makeObj("", "mrow", "", [||])
        | Some(below) => makeObj("", "mrow", "", [|
          below->toMathMLObject,
        |])
      },
    |])
    | _ => makeObj(idStr, "mi", valueStr, [||]);
    };

  };

  let mmlObjOfNode(node: node('a)) = {
    makeObj("", "math", "", [|
      node->toMathMLObject
    |]);
  };
