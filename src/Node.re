
open Util;

type id = string;
type path = list(Relations.t);
type nodeType = | PLAIN | FRAC | ROOT | TABLE;
type t('a, 'b) = {
  id: option(id),
  nodeType: nodeType,
  rPath: path,
  // parent: option((Relations.t, t('a))),
  value: option('a),
  relations: Relations.relations(t('a, 'b)),
  misc: option('b),
  flagMatch: bool,
  flagDelete: bool,
};

type opResult('a, 'b) = (option(t('a,'b)), array(t('a,'b)));

let getRelation(node: t('a, 'b), r) = node.relations->Relations.getRelation(r);
let setRelation(node: t('a, 'b), r, v: option(t('a, 'b))) = {
  ...node,
  relations: node.relations->Relations.setRelation(r, v),
};

let getPath(node) = node.rPath -> List.rev;
let pathStr(path) = path
  |> List.map(Relations.toString)
  |> String.concat(" ");

let getPathStr(node) = node
  |> getPath
  |> pathStr;

let empty : t('a, 'b) = {
  id: None,
  nodeType: PLAIN,
  rPath: [],
  value: None,
  relations: Relations.empty,
  misc: None,
  flagMatch: false,
  flagDelete: false,
};

let make(id, value) : t('a, 'b) = {
  ...empty,
  id: Some(id),
  value: Some(value),
};

let makeTyped(id, nodeT, value) : t('a, 'b) = {
  ...make(id, value),
  nodeType: nodeT,
};

let flagMatch(n) = { ...n, flagMatch: true };
let flagUnMatch(n) = { ...n, flagMatch: false };
let flagDelete(n) = { ...n, flagDelete: true };
let flagUnDelete(n) = { ...n, flagDelete: false };


let rec equal(na, nb) : bool = {
  na.id == nb.id
  && na.value == nb.value
  && relEqual(na.relations, nb.relations);
}
and relEqual(aRels, bRels) = {
  List.map2(
    (a,b) => a == b,
    aRels -> Relations.toList,
    bRels -> Relations.toList,
  )
  |> List.fold_left( (acc, v) => acc && v, true);
  // relations
  // |> Relations.toList
  // |> List.map
};

let rec findFirst(f: t('a, 'b) => bool, n: t('a, 'b)) : option(t('a, 'b)) = n->f
  ? Some(n)
  : n.relations |> Relations.fold(
    (acc, n'_opt) => switch(n'_opt) {
      | None => acc
      | Some(n') => switch(findFirst(f, n')) {
        | None => acc
        | Some(n'') => Some(n'')
      }
    },
    None,
  );

let relationsList(node: t('a, 'b)) = node.relations
  |> Relations.toList;

let existingRelationsList(node: t('a, 'b)) = node
  |> relationsList
  |> List.fold_left( (acc, (r, n_opt)) => switch(n_opt) {
    | None => acc
    | Some(n) => [(r, n), ...acc]
    }, []);

let rec mapList(f: t('a, 'b) => 'c, node: t('a, 'b)) = node
  |> relationsList
  |> List.fold_left( (acc, (_, n_opt)) => switch(n_opt) {
    | None => acc
    | Some(n) => acc @ mapList(f, n)
    }, [node->f]);

let mapArray(f: t('a, 'b) => 'c, node: t('a, 'b)) =
  node |> mapList(f) |> Array.of_list;

let valueString(node: t(string, 'b)) = node
  |> mapList(n => switch (n.value) {
    | None => ""
    | Some(v) => v
    })
  |> String.concat("");
// let rec valueString(node: t(string)) = node.relations
//   |> Relations.toList
//   |> List.fold_left( (acc, (_, n_opt)) => switch(n_opt) {
//     | None => acc
//     | Some(n) => acc ++ valueString(n);
//   }, switch(node.value) {
//     | None => ""
//     | Some(v) => v
//   })
// ;

let hasId(id, node: t('a, 'b)) = node
  |> mapList(n => n)
  |> List.exists(n => n.id == Some(id));

let rec setPaths(path, root) = {
  ...root,
  rPath: path,
  relations: root.relations
    |> Relations.mapr( (n_opt, r) => switch(n_opt) {
      | None => None
      | Some(n) => Some(setPaths([r, ...path], n))
    }),
};

let relate(r, b, a) = a->setRelation(r,
  Some(setPaths([r, ...a.rPath], b)));

let relateOpt(r, b_opt, a) = a->setRelation(r,
  switch(b_opt) {
    | None => None
    | Some(b) => Some(setPaths([r, ...a.rPath], b))
  });

let insertRelation(r, insertion, node) =
  switch(node->getRelation(r)) {
    | None => node |> relate(r, insertion)
    | Some(next) => node |> relate(r, insertion |> relate(r, next))
  };

let rec getNodeAt(path, root: t('a, 'b)) : option(t('a, 'b)) = switch(path) {
  | [] => Some(root)
  | [next, ...rest] => root->getRelation(next)
    |> fun
      | None => None
      | Some(n) => n |> getNodeAt(rest)
  };

let rec doAtPath_unsafe(f, path, node: t('a, 'b)) = switch(path) {
  | [] => f(node)
  | [hd, ...tl] => switch(node->getRelation(hd)) {
    | None => raise(Not_found);
    | Some(next) => node |> relate(hd, doAtPath_unsafe(f, tl, next))
  }
};

let rec doAtPath(f, path, node: t('a, 'b)) = switch(path) {
  | [] => f(node)
  | [hd, ...tl] => switch(node->getRelation(hd)) {
    | None => node;
    | Some(next) => node |> relate(hd, doAtPath(f, tl, next))
  }
};

let relateAtOpt(path, r, node_opt: option(t('a, 'b)), context: t('a, 'b)) = context
  |> doAtPath(n => n |> relateOpt(r, node_opt), path);

let relateAt(path, r, node: t('a, 'b), context: t('a, 'b)) = context
  |> doAtPath(n => n |> relate(r, node), path);

let replaceAt(path, replacement: t('a, 'b), context: t('a, 'b)) = switch (path -> List.rev) {
  | [] => replacement
  | [r, ...rPath] => {
      let nodePath = rPath -> List.rev;
      context |> relateAt(nodePath, r, replacement)
    }
  };

let replaceAtOpt(path, replacement_opt: option(t('a, 'b)), context: t('a, 'b)) =
switch (path -> List.rev) {
  | [] => replacement_opt
  | [r, ...rPath] => {
      let nodePath = rPath -> List.rev;
      Some(context |> relateAtOpt(nodePath, r, replacement_opt))
    }
};

let getRoot(subtree) = { ...subtree, relations: Relations.empty };

let rec fold(f: ('c, t('a, 'b)) => 'c, acc: 'c, node: t('a, 'b)) =
  Relations.fold(
    (acc', r_opt) => switch (r_opt) {
      | None => acc'
      | Some(next) => next |> fold(f, acc')
    },
    acc,
    node.relations
  ) -> f(node);

let rec map(f: t('a, 'b) => t('a, 'b), node: t('a, 'b)) = {
  ...node,
  relations: node.relations
  |> Relations.map(fun
    | None => None
    | Some(next) => Some(next |> map(f))
  ),
}->f;

let forEach(f: t('a, 'b) => 'c, node: t('a, 'b)) : unit = node
  |> fold(
    (_, n) => n->f->ignore,
    (),
  );

// fold 2 inorder bottom up
let rec fold2(
  f: ('c, option(t('a, 'b)), option(t('a, 'b))) => 'c,
  init: 'c,
  node1: t('a, 'b),
  node2: t('a, 'b)
  ) : 'c = f(
    Relations.fold2(
      (acc, r1_opt, r2_opt) => switch (r1_opt, r2_opt) {
      | (Some(next1), Some(next2)) => fold2(f, acc, next1, next2)
      | (Some(next1), None) => next1
        |> fold( (acc', n1) => f(acc', Some(n1), None), acc)
      | (None, Some(next2)) => next2
        |> fold( (acc', n2) => f(acc', None, Some(n2)), acc)
      | (None, None) => f(acc, None, None)
      },
      init,
      node1.relations,
      node2.relations,
    ),
    Some(node1),
    Some(node2)
  );

let rec map2(
  f: (option(t('a, 'b)), option(t('a, 'b))) => option(t('a, 'b)),
  node1: t('a, 'b),
  node2: t('a, 'b)
  ) : option(t('a, 'b)) = {

    let relations' = Relations.map2(
      (r1_opt, r2_opt) => switch (r1_opt, r2_opt) {
      | (Some(next1), Some(next2)) => map2(f, next1, next2)
      | (n1_opt, n2_opt) => f(n1_opt, n2_opt)
      },
      node1.relations,
      node2.relations,
    );

    f(
      Some({ ...node1, relations: relations' }),
      Some({ ...node2, relations: relations' })
    )
  };

let forEach2(
  f: (option(t('a, 'b)), option(t('a, 'b))) => 'c,
  node1: t('a, 'b),
  node2: t('a, 'b)
) : unit =
  fold2(
    (_, n1, n2) => f(n1, n2)->ignore,
    (),
    node1,
    node2
  );

let size(n) = n |> fold((count, _) => count + 1, 0);

let findParentOf(node: t('a, 'b), context: t('a, 'b)) = context
  |> fold((found, cur) => cur.relations
    |> Relations.findr_opt((_, r_opt) => switch (r_opt) {
      | None => false
      | Some(next) => (next.id == node.id)
    })
    |> fun
      | None => found
      | Some(_) => Some(cur)
    , None);

let getById(id, context) = context
  |> fold( (acc, n) => (n.id == Some(id)) ? Some(n) :acc,
    None,
  );

let rec string_of_node(s_of_a, node) = switch(node.nodeType) {
  | FRAC => {
    let nodeStr = switch(node.relations.above) {
      | None => "\\frac{}"
      | Some(above) => "\\frac{" ++ string_of_node(s_of_a, above) ++ "}"
    } ++ switch(node.relations.below) {
      | None => "{}"
      | Some(below) => "{" ++ string_of_node(s_of_a, below) ++ "}"
    };

    {
      ...node.relations,
      above: None,
      below: None,
    }
    |> string_of_relations(s_of_a, nodeStr);
  }
  | ROOT => {
    let nodeStr = "\\sqrt" ++ switch(node.relations.preSuper) {
      | None => ""
      | Some(preSuper) => {
        let rs = string_of_node(s_of_a, preSuper);
        (preSuper->size > 1)
          ? "[{" ++ rs ++ "}]"
          : "[" ++ rs ++ "]";
      }
    } ++ switch(node.relations.contains) {
      | None => "{}"
      | Some(contains) => "{" ++ string_of_node(s_of_a, contains) ++ "}"
    };

    {
      ...node.relations,
      contains: None,
      preSuper: None,
    }
    |> string_of_relations(s_of_a, nodeStr);
  }
  | TABLE => {
    let nodeStr = "\\begin{matrix}\n" ++ switch(node.relations.contains) {
      | None => ""
      | Some(contains) => string_of_node(s_of_a, contains)
    } ++ "\n\\end{matrix}";

    {
      ...node.relations,
      contains: None,
    }
    |> string_of_relations(s_of_a, nodeStr);
  }
  | _ => node.relations
    |> string_of_relations(s_of_a, switch (node.value) {
      | Some(v) => s_of_a(v)
      | None => ""
    })
}

and string_of_relations(s_of_a, default, relations) = relations
  |> Relations.foldr_rev(
    (s, n_opt, r) => switch(n_opt) {
      | None => s
      | Some(next) => {
        let rs = string_of_node(s_of_a, next);
        switch(r) {
        | ABOVE => "\\overset{" ++ rs ++ "}{" ++ s ++ "}"
        | BELOW => "\\underset{" ++ rs ++ "}{" ++ s ++ "}"
        | SUPER => s ++ "^{" ++ rs ++ "}"
        | SUB => s ++ "_{" ++ rs ++ "}"
        | RIGHT => s ++ " " ++ rs
        | CONTAINS => s ++ "{" ++ rs ++ "}"
        | ROW => s ++ " \\\\\n" ++ rs
        | COL => s ++ " & " ++ rs
        | _ => s
        };
      }
    },
    default
  );


let rootSubtree(subtree) = subtree |> setPaths([]);
let rootSubtrees(subtrees) = subtrees |> List.map(rootSubtree);

let rec _mark(
  pattern,
  markNode: t('a, 'b) => t('a, 'b),
  eq: (t('a, 'b), t('a, 'b)) => bool,
  tree: t('a, 'b),
) = if (eq(pattern, tree)) {
  {
    ...tree->markNode,
    relations: Relations.map2(
      (p_opt, n_opt) => switch(p_opt, n_opt) {
        | (None, None) => None
        | (None, Some(n)) => Some(n)
        | (Some(_), None) => None
        | (Some(p), Some(n)) => if (eq(p, n)) {
          _mark(p, markNode, eq, n)->Some
        } else {
          Some(n)
        }
      },
      pattern.relations,
      tree.relations,
    )
  }
} else {
  tree;
};

let markPattern(
  pattern,
  markNode: t('a, 'b) => t('a, 'b),
  eq: (t('a, 'b), t('a, 'b)) => bool,
  tree: t('a, 'b),
) = switch(findFirst(n => n->eq(pattern), tree)) {
  | None => tree
  | Some(first) => tree
    |> doAtPath(
      n => _mark(pattern, markNode, eq, n),
      first->getPath,
    )
};

let markValue(
  pattern,
  markNode: t('a, 'b) => t('a, 'b),
  tree: t('a, 'b),
) = markPattern(pattern, markNode, (p,t) => (p.value == t.value), tree);
let markId(
  pattern,
  markNode: t('a, 'b) => t('a, 'b),
  tree: t('a, 'b),
) = markPattern(pattern, markNode, (p,t) => (p.id == t.id), tree);

let markMatch(pattern, tree) = markValue(pattern, flagMatch, tree);
let markForDeletion(pattern, tree) = markId(pattern, flagDelete, tree);

let rec deleteFlagged(floating, tree: t('a, 'b)) = {
  let (relations', floating') = Relations.foldr(
    ((relations', floating'), n_opt, r) => switch(n_opt) {
      | None => (relations', floating')
      | Some(n) => {
        let (primary_opt, floating'') = deleteFlagged(floating, n);

        (
          relations'->Relations.setRelation(r, primary_opt),
          floating'' @  floating',
        )
      }
    },
    (Relations.empty, floating),
    tree.relations,
  );

  if (tree.flagDelete) {
    let (primary', floating') = Relations.foldr(
      ((p', f'), n_opt, r) => switch(n_opt) {
        | None => (p', f')
        | Some(n) => switch(r) {
          | Relations.RIGHT =>  (Some(n), f')
          | _ => (p', [n, ...f'])
        }
      },
      (None, floating'),
      relations',
    );
    (primary', floating');
  } else {
    (
      Some({ ...tree, relations: relations' }),
      floating',
    )
  }
};


module Match = {
  // Match a provided tree (pattern) as a subtree in a tree (context)
  // Note: Only exact match of trees for now (no wildcards etc).

  type n('a, 'b) = t('a, 'b);

  let eq(a: n('a, 'b), b: n('a, 'b)) = {
    a.value == b.value;
  };
  let opt_eq(a_opt, b_opt) = switch(a_opt, b_opt) {
    | (Some(a), Some(b)) => a->eq(b)
    | _ => false
  };

  let isExactMatch(pattern, tree) = fold2(
    (acc, p_opt, n_opt) => acc && switch(p_opt, n_opt) {
      // each node in pattern must equal node in tree
      | (None, None) => true
      | (Some(p), Some(n)) => p->eq(n)
      | _ => false
    },
    true,
    pattern,
    tree,
  );

  let isDirectMatch(pattern, tree) = fold2(
    (acc, p_opt, n_opt) => acc && switch(p_opt) {
      // anything matches None pattern
      | None => true
      | Some(p) => switch(n_opt) {
        | None => false
        // each node in pattern must equal node in tree
        | Some(n) => p->eq(n)
      }
    },
    true,
    pattern,
    tree,
  );

  let trim(pattern, n) = map2(
    (p_opt, n_opt) => p_opt->opt_eq(n_opt)
      ? n_opt : None,
    pattern,
    n,
  );

  // Cut subtree of n in shape of pattern
  let rec cutShape(pattern: n('a, 'b), n: n('a, 'b)) : (n('a, 'b), list(n('a, 'b))) =
    Relations.foldr2(
      // Examine each subpattern with each subnode
      ((base, trimmings), sp_opt, sn_opt, rel_t) => switch(sp_opt, sn_opt) {
        // No subpattern here and no existing subnode: ignore
        | (None, None) => (base, trimmings)
        // No subpattern here but existing subnode
        | (None, Some(sn)) => (
          // cut subnode from base
          base->setRelation(rel_t, None),
          // add subnode to trimmings
          [sn, ...trimmings]
        )
        // Subpattern here but no existing subnode: ignore
        | (Some(_), None) => (base, trimmings)
        // Subpattern here and existing subnode: cut subpattern from subnode
        | (Some(sp), Some(sn)) => {
          let (subBase, subTrimmings) = cutShape(sp, sn);
          (
            // connect base to (potentially cut) subBase
            base->setRelation(rel_t, Some(subBase)),
            // Add subtrimmings to trimmings
            subTrimmings @ trimmings,
          )
        }
      },
      // Begin with raw node n and no trimmmings
      (n, []),
      pattern.relations,
      n.relations,
    );

  let attachTrimmings(base, trimmings) = trimmings
  |> List.fold_left(
    ((context', trimmings'), trimming: n('a, 'b)) => switch(trimming.rPath) {
      | [] => (context', trimmings')
      | [r, ...tl] => switch(context' |> getNodeAt(trimming->getPath)) {
        | None => try(
          (
            // Try to relate trimming to replacement at path
            context'
            |> doAtPath_unsafe(
              n'=> n' |> relate(r, trimming),
              tl->List.rev),
            trimmings',
          )
        ) {
          // Add trimming to list of trimmings which couldn't be reconnected
          | Not_found => (context', [trimming, ...trimmings'])
        }
        // Replacement overwrites this trimming
        | Some(_) => (context', [trimming, ...trimmings'])
      }
    },
    (base, []),
  );

  // Replace subtree of n in shape of pattern with replacement
  let replaceShape(pattern: n('a, 'b), replacement: n('a, 'b), n: n('a, 'b))
  : (n('a, 'b), list(n('a, 'b))) = {
    // collect trimmings of pattern cut from n
    let (_, trimmings) = cutShape(pattern, n |> rootSubtree);

    // Relate pattern trimmings to replacement
    replacement->attachTrimmings(trimmings);
  };

  let directMatch(pattern, tree) = pattern->isDirectMatch(tree)
    ? pattern->cutShape(tree)->fst->Some
    : None;

  let optOr(opt, default) = switch(opt) {
    | None => default
    | Some(_) => opt
  };

  let matchFirst(pattern: n('a, 'b), context: n('a, 'b)) =
    fold(
      (acc, n) => switch(acc) {
        | Some(_) => acc
        | None => pattern->directMatch(n)->optOr(acc)
      },
      None,
      context,
    );

  let match(pattern: n('a, 'b), context: n('a, 'b)) =
    fold(
      (acc, n) => pattern->directMatch(n)->optOr(acc),
      None,
      context,
    );


  let rec _matchAll(pattern, matches, next) = next
    |> List.fold_left(
      (acc, node) => if (pattern->isDirectMatch(node)) {
        let (match, trimmings) = cutShape(pattern, node);

        _matchAll(pattern, [match, ...acc], trimmings)
      } else {
        _matchAll(
          pattern,
          acc,
          node->existingRelationsList
          |> List.map(snd)
        )
      },
      matches
    );

  let matchAll(pattern: n('a, 'b), context: n('a, 'b)) =
    _matchAll(pattern, [], [context])
    |> List.rev;
    // Node.fold(
    //   (acc, n) => pattern->directMatch(n)
    //     |> fun
    //       | None => acc
    //       | Some(n') => [n', ...acc],
    //   [],
    //   context,
    // );


  let find(pattern: n('a, 'b), context: n('a, 'b)) =
    match(pattern, context)
    |> fun
    | None => None
    | Some(m) => Some(m->getPath);

  let findAll(pattern: n('a, 'b), context: n('a, 'b)) =
    matchAll(pattern, context)
    |> List.map((m: n('a, 'b)) => m->getPath);

  let has(pattern: n('a, 'b), context: n('a, 'b)) =
    match(pattern, context)
    |> fun
    | None => false
    | Some(_) => true;

  let replaceSubtree(selected: option(n('a, 'b)), replacement, context: option(n('a, 'b))) =
  switch context {
    // replace empty context with replacement
    | None => (replacement, [||])
    | Some(contextTree) => switch selected {
      // empty selection => context unchanged
      | None => (context, [||])
      | Some(selectedTree) => contextTree
        |> getNodeAt(selectedTree->getPath)
        |> fun
        | None => (context, [||])
        | Some(sharedRoot) => {
          // perform operation in a rooted context
          let opResults = switch(replacement) {
            | None => sharedRoot
              |> cutShape(selectedTree);
            | Some(replaceTree) => sharedRoot
              |> replaceShape(selectedTree, replaceTree)
          };

          // relate result in context if necessary
          let result = switch(sharedRoot.rPath) {
            | [] => opResults
            | [r, ...tl] => (
              // relate result in context
              // to None if replacement is None
              // or to the primary result of the operation
              contextTree
              |> relateAtOpt(tl->List.rev, r,
                (replacement == None) ? None : opResults->fst->Some
              ),
              opResults->snd
            )
          };
          (
            result->fst->Some,
            // Update floating subtree paths s.t. each root is []
            result->snd |> List.rev_map(rootSubtree) |> Array.of_list,
          )
        };
    }
  };

  let replace(pattern: n('a, 'b), replacement: n('a, 'b), context: n('a, 'b)) = {
    let selected = match(pattern, context);
    replaceSubtree(selected, Some(replacement), Some(context));
  };

}



let rec allOutgoingEdgePaths(paths, rPrefix, tree) = tree
  |> relationsList
  |> List.fold_left( (acc, (r, n_opt)) => switch(n_opt) {
    | None =>
      [ List.rev_append(tree.rPath @ rPrefix, [r]), ...acc]
    | Some(n) => n |> allOutgoingEdgePaths(acc, rPrefix)
    },
    paths);

let rec allExistingOEPs(tree, paths, rPrefix, context) = tree
  |> relationsList
  |> List.fold_left( (acc, (r, n_opt)) => switch(n_opt) {
    | None => switch( context |> getNodeAt(tree.rPath @ rPrefix)) {
      | None => acc
      | Some(existingNode) =>
        [ List.rev_append(existingNode.rPath, [r]), ...acc]
      }
    | Some(n) => n -> allExistingOEPs(acc, rPrefix, context)
    },
    paths);


let outgoingEdgePaths(node) = node
  |> relationsList
  |> List.filter( ((_, opt)) => opt != None)
  |> List.map( ((_, n_opt)) => unsafe_some(n_opt)->getPath );

let pathEq(pathA, pathB) = try (
  List.fold_left2(
    (acc, a: Relations.t, b: Relations.t) => acc && a == b,
    true,
    pathA,
    pathB,
  )
  ) { | Invalid_argument(_) => false };

let pathStar(path: path) : path = path
  |> List.fold_left(
    (acc, cur) => switch(cur) {
      // ignore RIGHT relations
      | Relations.RIGHT => acc
      | _ => [cur, ...acc]
    }, []);

let pathStarEq(pathA, pathB) = pathEq(pathA->pathStar, pathB->pathStar);

let pathIntersect(aPaths, bPaths) = {
  aPaths
  |> List.partition( ap => bPaths
    |> List.fold_left((acc, bp) => acc || pathEq(ap, bp), false))
};

// type triple('a,'b,'c) = TRIPLE('a, 'b, 'c);
// let tfst((v,_,_)) = v;
// let tsnd((_,v,_)) = v
// let tthrd((_,_,v)) = v;

// let intersection(nodeA, nodeB) =
//   fold2(
//     (acc, nextA_opt, nextB_opt) => (
//       [nextA_opt, ...acc->tfst],
//       [nextA_opt, ...acc->tsnd],
//       [nextB_opt, ...acc->tthrd],
//     ),
//     // eq, +, -
//     ([], [], []),
//     nodeA,
//     nodeB
//   );

let isAllRight(path) = path
  |> List.fold_left((acc, b)=> acc && b == Relations.RIGHT, true);

let rec _pathPrefixREq(acc, pathA, pathB) = switch(pathA, pathB) {
  | ([], b') => acc && b'->isAllRight
  | (a', []) => acc && a'->isAllRight
  | ([aH, ...aT], [bH, ...bT]) => _pathPrefixREq(acc && aH == bH, aT, bT)
};

let pathPrefixREq(pathA, pathB) = _pathPrefixREq(true, pathA, pathB);

let pathIntersectPrefixR(aPaths, bPaths) = {
  aPaths
  // map each aPath to (aPath, optional first bPath that has pathPrefixREq)
  |> List.map( ap => bPaths
    |> List.fold_left((result, bp) => switch(result) {
        | None => pathPrefixREq(ap, bp) ? Some(bp) : result;
        | Some(_) => result;
      }, None)
    |> bP_opt => (ap, bP_opt)
  )
  // split aPath, option bPath based on bPath existing
  // first list: aPath, bPath pairs; second list: aPaths
  |> List.fold_left(
    ((accMatch, accNoMatch), (ap, bp_opt)) => switch(bp_opt) {
      | None => (accMatch, [ap, ...accNoMatch])
      | Some(bp) => ([(ap, bp), ...accMatch], accNoMatch)
    },
    ([], []),
  );
};


let rec greedyMatch(acc, a, b) = switch (a, b) {
  | ([aH, ...aT], [bH, ...bT]) => (aH == bH)
    ? greedyMatch([aH, ...acc], aT, bT)
    : acc
  | _ => acc
};

let rec ignorePrefix(pre, path) = switch(pre) {
  | [] => path
  | [_, ...tl] => ignorePrefix(tl, path->List.tl)
};

let relativePath(prefix, path) = {
  let rPre = prefix;// -> List.rev;
  let rPath = path;// -> List.rev;

  // let prefixMatch = greedyMatch([], rPre, rPath);
  let noPrefix = ignorePrefix(rPre, rPath)
    //|> List.rev;

  noPrefix;
};

// 2. Replace
//   - replace node value : node, value => t
let replaceNodeValue(node, value) = {
  ...node,
  value: value,
};
let rec foldPath(
  f: ('c, Relations.t, option(t('a, 'b))) => 'c,
  path: path,
  acc: 'c,
  n: t('a, 'b)
) = switch(path) {
  | [] => acc
  | [hd, ...tl] => {
    let next = n->getRelation(hd);
    let acc' = next |> f(acc, hd);
    switch(next) {
    | None => acc'
    | Some(n') => foldPath(f, tl, acc', n')
    }
  }
};

let rec fold2Path(
  f: ('c, Relations.t, option(t('a, 'b)), option(t('a, 'b))) => 'c,
  rPath: path,
  acc: 'c,
  nA: t('a, 'b),
  nB: t('a, 'b),
) = switch(rPath) {
  | [] => acc
  | [hd, ...tl] => {
    let nextA = nA->getRelation(hd);
    let nextB = nB->getRelation(hd);
    let acc' = f(acc, hd, nextA, nextB);
    switch(nextA, nextB) {
    | (Some(nA'), Some(nB')) => fold2Path(f, tl, acc', nA', nB')
    | _ => acc'
    }
  }
};

let array_of_paths(oeps) = oeps
  |> Array.of_list
  |> Array.map(Array.of_list);


let rec foldRelation(r, f, acc, node) =
switch(node->getRelation(r)) {
  | None => f(acc, node)
  | Some(next) => r->foldRelation(f, f(acc, node), next)
};

let rowNodes(node) = Relations.ROW
  ->foldRelation((a, n) => [n, ...a], [], node)
  ->List.rev;
let colNodes(node) = Relations.COL
  ->foldRelation((a, n) => [n, ...a], [], node)
  ->List.rev;

let _nRows(node, c) = Relations.ROW->foldRelation((a, _) => a+1, c, node);
let _nColumns(node, c) = Relations.COL->foldRelation((a, _) => a+1, c, node);

let nRows(node) = switch(node.nodeType) {
  | TABLE => switch(node->getRelation(Relations.CONTAINS)) {
    | None => 0
    | Some(firstRow) => firstRow->_nRows(0);
  }
  | _ => node->_nRows(0);
};

let maxColumnsInAllRows(node) = node
|> rowNodes
|> List.map(rowNode => rowNode->_nColumns(0))
|> List.fold_left(max, 0);

let nColumns(node) = switch(node.nodeType) {
  // for a matrix nColumns = max # of columns in any row
  | TABLE => switch(node->getRelation(Relations.CONTAINS)) {
    | None => 0
    | Some(firstRow) => firstRow->maxColumnsInAllRows;
  }
  | _ => node->maxColumnsInAllRows;
};

let rec foldRelationIf(r, test, f, acc, node) = test(acc, node)
// continue folding while test passes
? switch(node->getRelation(r)) {
  | None => f(acc, node)
  | Some(next) => r->foldRelationIf(test, f, f(acc, node), next)
}
// otherwise stop and return this result
: f(acc, node);

let nthRelation(r, n, node) = r->foldRelationIf(
  // test: fold while c < n
  ((c, _), _) => c < n,
  // f: increment c, and update current node
  ((c, _), cur) => (c+1, cur),
  // initial state (c: 0, result: node)
  (0, node),
  node,
)
// return node portion of result
|> ((_, nthNode)) => nthNode;

// List of the (column) nodes in row i
let nodesInRow(i, tableNode) =
switch(tableNode->getRelation(Relations.CONTAINS)) {
  | None => []
  | Some(firstRow) => {
    Relations.ROW->nthRelation(i, firstRow)->colNodes;
  }
};

// List of the (column) nodes in column i
let nodesInColumn(i, tableNode) =
switch(tableNode->getRelation(Relations.CONTAINS)) {
  | None => []
  | Some(firstRow) => {
    // find the ith column node in each row
    firstRow->rowNodes
    |> List.map(rowNode => Relations.COL->nthRelation(i, rowNode));
  }
};

let tableCell(i, j, tableNode) =
switch(tableNode->getRelation(Relations.CONTAINS)) {
  | None => None
  | Some(firstRow) => {
    // find the ith row node in each row
    let cell = Relations.ROW->nthRelation(i, firstRow)
    |> Relations.COL->nthRelation(j);

    Some(cell);
  }
};

// Get the coordinates of the node at rPath in table
// Note: rPath = reverse path so we are 'walking' upwards
// Result:
//  the coordinates of the parent table cell node
//  the path to the table root node
let rec _rPathTableCoords((nRows, nCols), rPath) = switch(rPath) {
  | [] => ((nRows, nCols), rPath)
  | [Relations.CONTAINS, ...tl] => ((nRows, nCols), tl)
  | [Relations.COL, ...tl] => (nRows, nCols+1)->_rPathTableCoords(tl)
  | [Relations.ROW, ...tl] => (nRows+1, nCols)->_rPathTableCoords(tl)
  // Ignore local relations until a CONTAINS, ROW, or COL relation is reached
  | [_, ...tl] => (nRows, nCols)->_rPathTableCoords(tl)
};
let rPathTableCoords(rPath) = (0, 0)->_rPathTableCoords(rPath);

let cellPosition(node) = rPathTableCoords(node.rPath)->fst;

// get the table node path (containing the node in context)
// and the coordinates of the node's (parent) cell in the table
let tableNodeOf(node) = {
  let (coord, rPath) = rPathTableCoords(node.rPath);
  (
    rPath->List.rev,
    coord,
  )
};

// let make an empty row size n
let rec makeRow(n) = n > 1
  ? empty |> relate(Relations.COL, makeRow(n-1))
  : empty;
let rec makeRows(n, m) = n > 1
  ? makeRow(m) |> relate(Relations.ROW, makeRows(n-1, m))
  : makeRow(m);

let rec _nTimesPath(acc, r, n) = n > 0
  ? [r, ...acc]->_nTimesPath(r, n-1)
  : acc;
let nTimesPath(r, n) = []->_nTimesPath(r, n);

let cellPath(coord) = Relations.ROW->nTimesPath(coord->fst)
  ->List.rev_append(Relations.COL->nTimesPath(coord->snd));

let rec mapEachR(f, r, node: t('a, 'b)) = switch(node->getRelation(r)) {
  | None => node->f;
  | Some(next) => node->f
    |> relate(r, next |> mapEachR(f, r))
};
let rec doRMost(f, r, node: t('a, 'b)) = switch(node->getRelation(r)) {
  | None => node->f;
  | Some(next) => node
    |> relate(r, next |> doRMost(f, r))
};

let mapRightMost(f, node: t('a, 'b)) = doRMost(f, Relations.RIGHT, node);

// Insert empty row in tableNode at row i
let insertRowsAt(tableNode, i, n, m) = {
  let emptyRows = makeRows(n, m);
  switch(tableNode->getRelation(Relations.CONTAINS)) {
    // Table was empty: add empty row node in table
    | None => tableNode
      |> relate(Relations.CONTAINS, emptyRows);
    | Some(firstRow) => tableNode
      |> relate(Relations.CONTAINS, firstRow
        |> doAtPath(
          // do insert the empty rows
          node => node
          |> relate(Relations.ROW, emptyRows
            |> doRMost(
              // relate lowest row in emptyRows to node's next row via ROW
              lowestRow => lowestRow |> relateOpt(Relations.ROW,
                node->getRelation(Relations.ROW)
              ),
              Relations.ROW,
            )
          ),
          // at path i ROWS in
          Relations.ROW->nTimesPath(i-1),
        )
      );
  };
};

let insertColumnsAt(tableNode, i, n) = {
  let emptyColumns = makeRow(n);
  switch(tableNode->getRelation(Relations.CONTAINS)) {
    // Table was empty: add empty row/col node in table
    | None => tableNode
      |> relate(Relations.CONTAINS, emptyColumns);
    | Some(firstRow) => {
      let rowOperation = if (i == 0) {
        // Insert empty column node in front of existing row node
        rowStart => emptyColumns
          |> doRMost(
            rightMostCol => rightMostCol
            // each row becomes empty columns -COL-> (rowStart without old ROW relation)
            |> relate(Relations.COL,
              rowStart->setRelation(Relations.ROW, None)),
            Relations.COL,
          )
      } else {
        // insert empty column node somewhere in row
        rowStart => rowStart
          |> doAtPath(
            // do insert the empty node
            node => node
            |> relate(Relations.COL, emptyColumns
              |> doRMost(
                // relate rightmost col in emptyColumns to node's next column
                rightmostCol => rightmostCol
                |> relateOpt(Relations.COL, node->getRelation(Relations.COL)),
                Relations.COL,
              )
            ),
            // at path i columns in
            Relations.COL->nTimesPath(i-1),
          );
      };

      tableNode
      |> relate(Relations.CONTAINS, firstRow
        |> mapEachR(rowOperation, Relations.ROW)
      );
    }
  };
};

//   - closest node, nodes in rect -> subtree selection vs matrix selection
//   - nodes in rect, closest node => selected subtree

let rec replaceColumnCells(start, replaceStart) = switch(
  start->getRelation(Relations.COL),
  replaceStart->getRelation(Relations.COL),
) {
  | (Some(nextCol), Some(nextReplaceCol)) => replaceStart
    |> relate(Relations.COL, nextCol->replaceColumnCells(nextReplaceCol));
  | (Some(nextCol), None) => replaceStart
    |> relate(Relations.COL, nextCol);
  | _ => replaceStart;
};

let rec replaceCells(start, replaceStart) = switch(
  start->getRelation(Relations.ROW),
  replaceStart->getRelation(Relations.ROW),
) {
  | (Some(nextRow), Some(nextReplaceRow)) =>
    start->replaceColumnCells(replaceStart)
    |> relate(Relations.ROW, nextRow->replaceCells(nextReplaceRow));
  | (Some(nextRow), None) =>
    start->replaceColumnCells(replaceStart)
    |> relate(Relations.ROW, nextRow);
  | _ => start->replaceColumnCells(replaceStart);
};


// recursively traverse right relations until a leaf
let rightMostNode(node) = Relations.RIGHT
  ->foldRelation((_, cur) => cur, node, node);

let insertRowAfter(node, child, context) = {
  let (tableNodePath, (rc, cc)) = tableNodeOf(node);
  let coord = (rc+1, cc);
  let pathToCell = cellPath(coord);

  context
  // first insert the empty rows
  |> doAtPath(
    tableNode => tableNode->insertRowsAt(
      coord->fst,
      child->nRows,
      max(
        child->nColumns,
        tableNode->nColumns,
      ),
    ),
    tableNodePath,
  )
  // then insert the child node
  |> doAtPath(
    cellNode => cellNode->replaceCells(child),
    tableNodePath @ [Relations.CONTAINS, ...pathToCell],
  );
};

let insertColumnAfter(node, child, context) = {
  let (tableNodePath, (rc, cc)) = tableNodeOf(node);
  let coord = (rc, cc+1);
  let pathToCell = cellPath(coord);

  context
  // first insert the empty column
  |> doAtPath(
    // tableNode => tableNode->insertColumnsAt(coord->snd, 1),
    tableNode => tableNode->insertColumnsAt(
      coord->snd,
      child->nColumns,
    )
    |> doAtPath(
      cellNode => cellNode->replaceColumnCells(
        child
        |> relateOpt(Relations.ROW, cellNode->getRelation(Relations.ROW))
      ),
      [Relations.CONTAINS, ...pathToCell],
    ),
    tableNodePath,
  );
};

let insert(node, r: Relations.t, child, context) = {
  context
  |> findFirst(n => n.id == node.id)
  |> fun
    | None => context
    | Some(nodeInContext) => {
      let next_opt = nodeInContext->getRelation(r);
      switch(r) {
      | Relations.ROW => switch(next_opt) {
        | None => insertRowAfter(nodeInContext, child, context)
        | Some(existing) => insertRowAfter(existing, child, context)
      }
      | Relations.COL => switch(next_opt) {
        | None => insertColumnAfter(nodeInContext, child, context)
        | Some(existing) => insertColumnAfter(existing, child, context)
      }
      | _ => context
      |> doAtPath(
        nodeInContext => nodeInContext
        // relate parent to child via r
        // Note: relate fixes paths to be relative to parent
        |> relate(r, switch(next_opt) {
          | None => child
          // if n had an existing r relation
          | Some(next) => child
            // relate rightmost node in child to existing via RIGHT
            |> mapRightMost(rm => rm |> relate(Relations.RIGHT, next))
        }),
        nodeInContext->getPath,
      )
    }
  }
};

let insertBefore(node, r, child, context) = {
  // parent -pnr-> node
  // becomes parent -pnr-> child -r-> node
  context
  |> findParentOf(node)
  |> fun
    | None => child
      |> setPaths(context.rPath)
      |> mapRightMost(rm => rm |> relate(r, context))
    | Some(parentInContext) => {
      let parentNodeRelation = switch(node.rPath) {
        | [] => r // node has a parent so rPath shouldn't be empty
        | [pnr, ..._] => pnr
      };
      let node_opt = parentInContext->getRelation(parentNodeRelation);
      switch(r) {
      | Relations.ROW =>
        insertRowAfter(parentInContext, child, context);
      | Relations.COL =>
        insertColumnAfter(parentInContext, child, context);
      | _ => context
      |> doAtPath(
        parentInContext => parentInContext
        |> relate(parentNodeRelation, child
          |> mapRightMost(rm => rm |> relateOpt(r, node_opt))),
        parentInContext->getPath,
      )
    }
  }
};

//   - replace node subtree : node, subtree => t
//   - replace subtree node : subtree, node => t
//   - replace subtree subtree : subtree, subtree => t
let replaceNodeSubtree(node, replacement, context: t('a, 'b)) = {
  // Note: node must be in context
  // Js.log(Js.Json.stringifyAny(node));
  // Js.log(Js.Json.stringifyAny(replacement));

  // Replace node at node.rPath with replacement in context
  let replacedContext = context
  |> replaceAt(node->getPath, replacement);

  // let nodeOEPs = node->outgoingEdgePaths;
  let nodeOEPs = node |> allOutgoingEdgePaths([], []);
  let replOEPs = replacement |> allOutgoingEdgePaths([], node.rPath);
  // nodeOEPs -> array_of_paths -> Js.log;
  // replOEPs -> array_of_paths -> Js.log;
  let (match, remainder) = pathIntersect(nodeOEPs, replOEPs);
  // Js.log("match:")
  // match -> array_of_paths -> Js.log;
  // Js.log("remainder:")
  // remainder -> array_of_paths -> Js.log;

  // Relate any of node's matching old outgoing edges
  match
  |> List.fold_left(
    (context', path) => switch(context |> getNodeAt(path)) {
      | None => context'
      | Some(n') => switch(path -> List.rev){
        | [] => context'
        | [r, ...tl] => context'
          |> relateAt(tl -> List.rev, r, n')
      }
    }, replacedContext)
  |> updatedContext => (
    updatedContext,
    remainder
    // Return any of node's unmatched remaining outgoing edges
    |> List.fold_left((acc, path) => switch(context |> getNodeAt(path)) {
      | None => acc
      // Update floating subtree paths s.t. root is []
      | Some(n) => [n->rootSubtree, ...acc]
    }, [])
    |> List.rev
    |> Array.of_list
  )
};

let replaceSubtree(selected, replacement, context) = switch context {
  // replace empty context with replacement
  | None => (replacement, [||])
  | Some(contextTree) => switch selected {
    // empty selection => context unchanged
    | None => (context, [||])
    | Some(selectTree) => {
      let selectOEPs = selectTree
        |> allOutgoingEdgePaths([], []);

      // empty replacement => delete selected
      // selection and replacement => replace selection with replacement
      let replOEPs = switch(replacement) {
      // replace with nothing has single OEP at selectTree
      | None => [ selectTree->getPath ];
      | Some(replaceTree) => replaceTree
        |> allOutgoingEdgePaths([], selectTree.rPath);
      };

      let getSomePathNodePairs(paths, contextTree) = paths
        |> List.fold_left(
          (acc, path) => switch(contextTree |> getNodeAt(path)) {
            | None => acc
            | Some(n) => [(path, n), ...acc]
          }, []);

      let (match, remainder) = pathIntersectPrefixR(selectOEPs, replOEPs)
      |> ((matchPathPairs, remainderPaths)) => {
        // Js.log("selectOEPS");
        // selectOEPs
        // |> List.iter( p => Js.log(p->pathStr));
        // Js.log("replOEPS");
        // replOEPs
        // |> List.iter( p => Js.log(p->pathStr));
        // Js.log("matchpairs");
        // matchPathPairs
        // |> List.iter( ((sp, rp)) => Js.log3(sp->pathStr, ",",rp->pathStr));
        // Js.log("remainders");
        // remainderPaths
        // |> List.iter( p => Js.log(p->pathStr));
        (
          matchPathPairs
          |> List.fold_left(
            (acc, (selectPath, replPath)) =>
            // fetch nodes from context at selectOEP
            switch(contextTree |> getNodeAt(selectPath)) {
              | None => acc
              | Some(n) => [(replPath, n), ...acc]
            }, []),
          // collect any existing remainder subtrees from context
          remainderPaths -> getSomePathNodePairs(contextTree),
        );
      }

      // Context with selected node replaced in context
      let replacedContextOpt = contextTree
      |> replaceAtOpt(selectTree->getPath, replacement);

      // Relate selection's matching subtrees in the replaced context
      let matchResult = switch(replacedContextOpt) {
        // if replaced context is empty check for OEP matches
        | None => switch (match) {
          // no matches => None
          | [] => None
          // a match => use first match's rootedSubtree
          | [hd, ..._] => Some(hd->snd->rootSubtree)
        }
        | Some(replacedContext) => Some(
          match
          |> List.fold_left(
            // relate each subtree match in replacedContext
            (context', (path, n)) => switch(path -> List.rev){
              // match is at relative root so examine select's path
              | [] => switch(selectTree.rPath) {
                // select was at root so use rooted match
                | [] => n->rootSubtree
                // relate match at position of select
                | [r, ...tl] => context'
                  |> relateAt(tl->List.rev, r, n)
              }
              // relate match at path in context
              | [r, ...tl] => context'
                |> relateAt(tl -> List.rev, r, n)
            }, replacedContext)
          )
      };

      (
        matchResult,
        // Return any subtrees at selection's unmatched outgoing edges
        // Update floating subtree paths s.t. each root is []
        remainder |> List.map(((_,n)) => n->rootSubtree)
          |> Array.of_list
      )

    };
  };
};

let rec _replaceAll(pattern, replacement, context, trimmings) = {
  switch(Match.matchAll(pattern, context) -> List.rev){
  | [] => (Some(context), trimmings |> Array.of_list)
  | [hd, ...tl] =>
    let subtree = Some(hd);
    let (newContext, newTrimmings) = replaceSubtree(subtree, Some(replacement), Some(context));
    switch(newContext) {
    | None => raise(Not_found);
    | Some(goodContext) => _replaceAll(pattern, replacement, goodContext, trimmings @ (newTrimmings |> Array.to_list))
    }
  }
}

let replaceAll(pattern, replacement, context) = switch context {
  | None => (replacement, [||])
  | Some(contextTree) => switch pattern {
    | None => (context, [||])
    | Some(patternTree) => switch replacement {
      | None => raise(Not_found);
      | Some(replacementTree) => _replaceAll(patternTree, replacementTree, contextTree, [])
    };   
  }; 

  
};

let deleteSubtree(node, context) = Some(context)
  |> replaceSubtree(Some(node), None);

let group(selection, left_opt, right_opt, context) = {
  Some(context)
  |> replaceSubtree(Some(selection), {
    let grouped = empty |> relate(Relations.CONTAINS, selection);

    let rDelimited = switch(right_opt) {
      | None => grouped
      | Some(right) => grouped |> relate(Relations.RIGHT, right);
    };

    let delimited = switch(left_opt) {
      | None => rDelimited
      | Some(left) => left |> relate(Relations.RIGHT, rDelimited);
    };

    Some(delimited);
  })
};

let makeRoot(selection, context) = {
  Some(context)
  |> replaceSubtree(Some(selection), {
    let rootNode : t('a, 'b) = {
      ...empty,
      id: None,
      value: None,
      nodeType: ROOT,
    };

    Some(rootNode |> relate(Relations.CONTAINS, selection));
  })
};


//   - replace all selected node : node, value => t
//   - replace all selected node w tree : node, subtree => t
//   - replace all selected subtree w node : subtree, node => t
//   - replace all selected subtree w subtree : subtree, subtree => t
//   - replace each in selection : subtree, subtree => t
//   - matrix add row(s)
//   - matrix add col(s)
//   - matrix check region spanning
//   - matrix del row(s)
//   - matrix del col(s)
//   - matrix compare region size
//   - replace matrix region : region, matrix => t
//   - replace matrix region : region, region => t
//   - replace each in matrix region : region, subtree => t
//   -



// ignore matrices for now
// type mfenced('a) = {
//   id: id,
//   left: string,
//   right: string,
//   contains: mtable('a),
// }
// and mtable('a) = {
//   id: id,
//   path: list(Relations.t),
//   root: mtr('a),
// }
// and mtr('a) = {
//   id: id,
//   row: option(mtr('a)),
//   col: option(mtd('a)),
// }
// and mtd('a) = {
//   id: id,
//   value: option('a),
//   col: option(mtd('a)),
// };
