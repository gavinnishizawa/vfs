
type indexedNode('a) = {
  i: int,
  node: 'a,
  relations: Relations.relations(indexedNode('a)),
};

type nodeIndex('a) = array(indexedNode('a));



let rec indexNode(node: 'a, getRelations, i) = {
  /**
   * Produce:
   *   subIndexes: [subindex, ...]
   *   iRelations: relations to sub index nodes
   *   i': new i value
   * from node relations
   */

  let (subIndexes, iRelations, i') = node->getRelations
  |> Relations.foldr_rev(
  /**
   * fold over node relations in reverse order so that
   * lower priority relations come after higher priority relations
   */
    ((subIndexes, iRelations, i'), next_opt, t) => switch(next_opt) {
      // No relation => continue folding
      | None => ( subIndexes, iRelations, i')
      | Some(next) => {
        // Next relation => produce: (subIndex, iNext, newI)
        let (subIndex, iNext, i'') = indexNode(next, getRelations, i' + 1);
        (
          // Add subIndex to subIndices
          [subIndex, ...subIndexes],
          // Add relation to next indexNode to iRelations
          iRelations->Relations.setRelation(t, Some(iNext)),
          // Update i'
          i'',
        )
      }
    },
    // accumulate (subIndexes, indexRelations, newI) starting with:
    ([], Relations.empty, i),
  );

  // create indexNode with i, node, and index relations
  let iNode = { i, node, relations: iRelations };

  /**
   * subIndexes are in reverse order since later subIndexes
   * are prepended ([new, ...olds]), so reverse before merging
   * into single index with iNode at front
   */
  let index = [iNode, ...subIndexes->List.rev->List.flatten];

  /**
   * return
   *   index: iNode then the merged subindexes put in order
   *   root: iNode
   *   new i: i'
   */
  (index, iNode, i');
};

let indexTree(tree: Node.t('a, 'b)) = {
  let (indexList, root, _size) = indexNode(tree, t=>t.relations, 0);

  (
    indexList -> Array.of_list,
    root,
  )
};

let indexAny(tree: 'a, getRelations) = {
  let (indexList, root, _size) = indexNode(tree, getRelations, 0);

  (
    indexList -> Array.of_list,
    root,
  )
};


let rec mapList(f, iNode: indexedNode('a)) = {
  iNode.relations
  |> Relations.toList
  |> List.fold_left( (acc, (_, n_opt)) => switch(n_opt) {
    | None => acc
    | Some(n) => acc @ mapList(f, n)
    }, [iNode->f]);
};

let rec iterEdges(f, iNode: indexedNode('a)) = {
  iNode.relations
  |> Relations.toList
  |> List.iter( ((r, n_opt)) => switch(n_opt) {
    | None => ignore()
    | Some(n) => {
      f(r, iNode, n);
      iterEdges(f, n);
    }
  });
};
