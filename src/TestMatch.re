
let testIsDirectMatch(a, b, target) = TestUtil.test(
  () => Node.Match.isDirectMatch(a, b),
  target,
);

let testIsDirectMatch0() = testIsDirectMatch(
  Node.make("a", "a"),
  Node.make("a", "a"),
  true
);

let testIsDirectMatch1() = testIsDirectMatch(
  Node.make("a", "a"),
  Node.make("b", "b"),
  false,
);

let testIsDirectMatch2() = testIsDirectMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  ),
  true,
);

let testIsDirectMatch3() = testIsDirectMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("c", "c")
  |> Node.relate(Relations.RIGHT, Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
  ),
  false,
);

let testIsDirectMatch4() = testIsDirectMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b"))
  |> Node.relate(Relations.RIGHT, Node.make("c", "c")),
  false,
);

let testIsDirectMatch5() = testIsDirectMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
  |> Node.relate(Relations.SUPER, Node.make("c", "c")),
  true,
);

let runIsDirectMatch() = {
  [
    testIsDirectMatch0,
    testIsDirectMatch1,
    testIsDirectMatch2,
    testIsDirectMatch3,
    testIsDirectMatch4,
    testIsDirectMatch5,
  ]
  |> TestUtil.runTests("IsDirectMatch");
};


let testIsExactMatch(a, b, target) = TestUtil.test(
  () => Node.Match.isExactMatch(a, b),
  target,
);


let testIsExactMatch0() = testIsExactMatch(
  Node.make("a", "a"),
  Node.make("a", "a"),
  true,
);

let testIsExactMatch1() = testIsExactMatch(
  Node.make("a", "a"),
  Node.make("b", "b"),
  false,
);

let testIsExactMatch2() = testIsExactMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  true,
);

let testIsExactMatch3() = testIsExactMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
  ),
  false,
);

let testIsExactMatch4() = testIsExactMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  false,
);

let testIsExactMatch5() = testIsExactMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")),
  Node.make("a", "a"),
  false,
);

let testIsExactMatch6() = testIsExactMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("+", "+")
        |> Node.relate(Relations.RIGHT, Node.make("1", "1"))
      )
    )
  )
  |> Node.relate(Relations.SUB, Node.make("d", "d")),
  // matches by value only
  Node.make("0", "a")
  |> Node.relate(Relations.SUPER, Node.make("1", "b")
    |> Node.relate(Relations.SUPER, Node.make("2", "c")
      |> Node.relate(Relations.RIGHT, Node.make("3", "+")
        |> Node.relate(Relations.RIGHT, Node.make("4", "1"))
      )
    )
  )
  |> Node.relate(Relations.SUB, Node.make("5", "d")),
  true,
);

let testIsExactMatch7() = testIsExactMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("+", "+")
        |> Node.relate(Relations.RIGHT, Node.make("1", "1"))
      )
    )
  )
  |> Node.relate(Relations.SUB, Node.make("d", "d")),
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("e", "e")
      |> Node.relate(Relations.RIGHT, Node.make("+", "+")
        |> Node.relate(Relations.RIGHT, Node.make("1", "1"))
      )
    )
  )
  |> Node.relate(Relations.SUB, Node.make("d", "d")),
  false,
);

let testIsExactMatch8() = testIsExactMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("+", "+")
        |> Node.relate(Relations.RIGHT, Node.make("1", "1"))
      )
    )
  )
  |> Node.relate(Relations.SUB, Node.make("d", "d")),
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("+", "+")
        |> Node.relate(Relations.RIGHT, Node.make("1", "1"))
      )
    )
    |> Node.relate(Relations.RIGHT, Node.make("-", "-")
      |> Node.relate(Relations.RIGHT, Node.make("2", "2"))
    )
  )
  |> Node.relate(Relations.SUB, Node.make("d", "d")),
  false,
);


let runIsExactMatch() = {
  [
    testIsExactMatch0,
    testIsExactMatch1,
    testIsExactMatch2,
    testIsExactMatch3,
    testIsExactMatch4,
    testIsExactMatch5,
    testIsExactMatch6,
    testIsExactMatch7,
    testIsExactMatch8,
  ]
  |> TestUtil.runTests("IsExactMatch");
};


let testMatch(a, b, target) = TestUtil.test(
  () => Node.Match.match(a, b),
  target,
);

let testMatch0() = testMatch(
  Node.make("a", "a"),
  Node.make("a", "a"),
  Some(Node.make("a", "a")),
);

let testMatch1() = testMatch(
  Node.make("a", "a"),
  Node.make("b", "b"),
  None,
);

let testMatch2() = testMatch(
  Node.make("a", "a"),
  Node.make("b", "b")
  |> Node.relate(Relations.RIGHT, Node.make("a", "a")),
  Some({
    ...Node.make("a", "a"),
    rPath: [Relations.RIGHT],
  }),
);

let testMatch3() = testMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("b", "b")
  |> Node.relate(Relations.SUPER, Node.make("a", "a")
    |> Node.relate(Relations.SUPER, Node.make("a", "a")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
    ),
  ),
  Some(
    {
      ...Node.make("a", "a"),
      rPath: [Relations.SUPER, Relations.SUPER],
    } |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  ),
);

let testMatch4() = testMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("a", "a")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
    ),
  ),
  Some(
    Node.make("a", "a")
    |> Node.relate(Relations.SUPER, Node.make("b", "b"))
  ),
);

let testMatch5() = testMatch(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
    ),
  ),
  Some(
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
  ),
);


let runMatch() = {
  [
    testMatch0,
    testMatch1,
    testMatch2,
    testMatch3,
    testMatch4,
    testMatch5,
  ]
  |> TestUtil.runTests("Match");
};


let testMatchAll(a, b, target) = TestUtil.test(
  () => Node.Match.matchAll(a, b),
  target,
);

let testMatchAll0() = testMatchAll(
  Node.make("a", "a"),
  Node.make("a", "a"),
  [
    Node.make("a", "a"),
  ],
);

let testMatchAll1() = testMatchAll(
  Node.make("a", "a"),
  Node.make("b", "b"),
  [],
);

let testMatchAll2() = testMatchAll(
  Node.make("a", "a"),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("a", "a")),
  [
    Node.make("a", "a"),
    {
      ...Node.make("a", "a"),
      rPath: [Relations.RIGHT],
    },
  ],
);

let testMatchAll3() = testMatchAll(
  Node.make("a", "a"),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a"))
  ),
  [
    Node.make("a", "a"),
    {
      ...Node.make("a", "a"),
      rPath: [Relations.RIGHT],
    },
    {
      ...Node.make("a", "a"),
      rPath: [Relations.RIGHT, Relations.RIGHT],
    },
  ],
);

let testMatchAll4() = testMatchAll(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("a", "a")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a"))
  ),
  [
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a"))
    // No overlapping matches ?
    // {
    //   ...Node.make("a", "a"),
    //   rPath: [Relations.RIGHT],
    // } |> Node.relate(Relations.RIGHT, Node.make("a", "a")),
  ],
);

let testMatchAll5() = testMatchAll(
  Node.make("a", "a"),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a"))
  ),
  [
    Node.make("a", "a"),
    {
      ...Node.make("a", "a"),
      rPath: [Relations.RIGHT, Relations.RIGHT],
    },
  ],
);

let runMatchAll() = {
  [
    testMatchAll0,
    testMatchAll1,
    testMatchAll2,
    testMatchAll3,
    testMatchAll4,
    testMatchAll5,
  ]
  |> TestUtil.runTests("MatchAll");
};


let testReplace(pattern, replacement, context, target) = TestUtil.test(
  () => Node.Match.replace(pattern, replacement, context), target
);

let abc = Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  );

let aec = Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("e", "e")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  );

let abcd = Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUB, Node.make("d", "d")
      )
    )
  );

let abbd = Node.make("a", "a")
|> Node.relate(Relations.RIGHT, Node.make("b", "b")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUB, Node.make("d", "d")
    )
  )
);

let aeed = Node.make("a", "a")
|> Node.relate(Relations.RIGHT, Node.make("e", "e")
  |> Node.relate(Relations.SUPER, Node.make("e", "e")
    |> Node.relate(Relations.SUB, Node.make("d", "d")
    )
  )
);

let abeab = Node.make("a", "a")
|> Node.relate(Relations.RIGHT, Node.make("b", "b")
  |> Node.relate(Relations.RIGHT, Node.make("e", "e")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      )
    )
  )
);

let ab = Node.make("a", "a")
|> Node.relate(Relations.RIGHT, Node.make("b", "b")
);

let cec = Node.make("c", "c")
|> Node.relate(Relations.RIGHT, Node.make("e", "e")
  |> Node.relate(Relations.RIGHT, Node.make("c", "c")
  )
);

let cbecb = Node.make("c", "c")
|> Node.relate(Relations.RIGHT, Node.make("b", "b")
  |> Node.relate(Relations.RIGHT, Node.make("e", "e")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      )
    )
  )
);

let testReplace0() = testReplace(
  Node.make("a", "a"),
  Node.make("b", "b"),
  Node.make("a", "a"),
  (Some(Node.make("b", "b")), [] |> Array.of_list)
);

let testReplace1() = testReplace(
  Node.make("a", "a"),
  Node.make("e", "e"),
  abcd,
  (Some(
    Node.make("e", "e")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      |> Node.relate(Relations.SUPER, Node.make("c", "c")
        |> Node.relate(Relations.SUB, Node.make("d", "d")
        )
      )
    )), [] |> Array.of_list)
);

let testReplace2() = testReplace(
  abc,
  Node.make("x", "x"),
  abcd,
  (Some(Node.make("x", "x")), [Node.make("d", "d")] |> Array.of_list)
);

let testReplace3() = testReplace(
  Node.make("b", "b"),
  Node.make("e", "e"),
  abc,
  (Some(aec), [] |> Array.of_list)
)

let runReplaceSubTree() = {
  [
    testReplace0,
    testReplace1,
    testReplace2,
    testReplace3,
  ]
  |> TestUtil.runTests("Match Replace");
}

let testReplaceAll(pattern, replacement, context, target) = TestUtil.test(
  () => Node.replaceAll(Some(pattern), Some(replacement), Some(context)),
  target -> (((tree, floating)) => (Some(tree), floating -> Array.of_list))
);

let testReplaceAll0() = testReplaceAll(
  Node.make("a", "a"),
  Node.make("b", "b"),
  Node.make("a", "a"),
  (Node.make("b", "b"), [])
);

let testReplaceAll1() = testReplaceAll(
  Node.make("b", "b"),
  Node.make("e", "e"),
  abbd,
  (aeed, [])
);

let testReplaceAll2() = testReplaceAll(
  Node.make("a", "a"),
  Node.make("c", "c"),
  abeab,
  (cbecb, [])
);

let testReplaceAll3() = testReplaceAll(
  ab,
  Node.make("c", "c"),
  abeab,
  (cec, [])
);

let runReplaceAll() = {
  [
    testReplaceAll0,
    testReplaceAll1,
    testReplaceAll2,
    testReplaceAll3,
  ]
  |> TestUtil.runTests("Match Replace All");
}

let run() = {
  runIsDirectMatch();
  runIsExactMatch();
  runMatch();
  runMatchAll();
  runReplaceSubTree();
  runReplaceAll();
};