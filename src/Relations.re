
type t = | ROW | COL
  | RIGHT | SUPER | SUB | ABOVE | BELOW
  | CONTAINS | PRESUPER;
type types_t = {
  right: t,
  super: t,
  sub: t,
  above: t,
  below: t,
  contains: t,
  preSuper: t,
  row: t,
  col: t,
};
let types = {
  right: RIGHT,
  super: SUPER,
  sub: SUB,
  above: ABOVE,
  below: BELOW,
  contains: CONTAINS,
  preSuper: PRESUPER,
  row: ROW,
  col: COL,
};
let order = [
  ROW, COL,
  RIGHT,
  SUB, SUPER,
  BELOW, ABOVE,
  CONTAINS, PRESUPER ];

type relations('a) = {
  right: option('a),
  super: option('a),
  sub: option('a),
  above: option('a),
  below: option('a),
  contains: option('a),
  preSuper: option('a),
  row: option('a),
  col: option('a),
};


let empty = {
  right: None,
  super: None,
  sub: None,
  above: None,
  below: None,
  contains: None,
  preSuper: None,
  row: None,
  col: None,
};

let toString(t: t) : string = switch(t) {
  | RIGHT => "right"
  | SUPER => "super"
  | SUB => "sub"
  | ABOVE => "above"
  | BELOW => "below"
  | CONTAINS => "contains"
  | PRESUPER => "preSuper"
  | ROW => "row"
  | COL => "column"
};

let getRelation(r: relations('a), t: t) : option('a) = switch(t) {
  | RIGHT => r.right
  | SUPER => r.super
  | SUB => r.sub
  | ABOVE => r.above
  | BELOW => r.below
  | CONTAINS => r.contains
  | PRESUPER => r.preSuper
  | ROW => r.row
  | COL => r.col
};

let setRelation(r: relations('a), t: t, v: option('a)) : relations('a) = switch(t) {
  | RIGHT => { ...r, right: v }
  | SUPER => { ...r, super: v }
  | SUB => { ...r, sub: v }
  | ABOVE => { ...r, above: v }
  | BELOW => { ...r, below: v }
  | CONTAINS => { ...r, contains: v }
  | PRESUPER => { ...r, preSuper: v }
  | ROW => { ...r, row: v }
  | COL => { ...r, col: v }
};

let _foldr(order, f: ('b, option('a), t) => 'b, acc: 'b, r: relations('a)) : 'b =
  order
  |> List.fold_left(
    (acc', t) => acc'->f(r->getRelation(t), t),
    acc);

let foldr(f: ('b, option('a), t) => 'b, acc: 'b, r: relations('a)) : 'b =
  order->_foldr(f, acc, r);

let foldr_rev(f: ('b, option('a), t) => 'b, acc: 'b, r: relations('a)) : 'b =
  order->List.rev->_foldr(f, acc, r);

let fold(f: ('b, option('a)) => 'b, acc: 'b, r: relations('a)) : 'b =
  foldr(
    (acc, opt, _) => f(acc, opt),
    acc,
    r
  );

let mapr(f: (option('a), t) => option('b), r: relations('a)) : relations('b) =
  foldr(
    (r', n_opt, t) => r'->setRelation(t, n_opt->f(t)),
    empty,
    r
  );

let map(f: option('a) => option('b), r: relations('a)) : relations('b) =
  mapr( (n_opt, _) => n_opt->f, r);

let toList(r: relations('a)) : list((t, option('a))) = order
  |> List.map(t => (t, r->getRelation(t)));

let findr_opt(f: (t, option('a)) => bool, r) : option((t, option('a))) = r
  |> toList |> List.find_opt( ((t, n_opt)) => f(t, n_opt) );

let foldr2(
  f: ('c, option('a), option('b), t) => 'c,
  acc: 'c,
  r1: relations('a),
  r2: relations('b),
  ) : 'c = order
  |> List.fold_left(
    (acc', t) =>  acc' -> f(
      r1->getRelation(t),
      r2->getRelation(t),
      t
    ),
    acc);

let fold2(
  f: ('c, option('a), option('b)) => 'c,
  acc: 'c,
  r1: relations('a),
  r2: relations('b),
  ) : 'c =
  foldr2(
    (acc', n1_opt, n2_opt, _) => f(acc', n1_opt, n2_opt),
    acc,
    r1,
    r2
  );

let map2(
  f: (option('a), option('b)) => option('c),
  r1: relations('a),
  r2: relations('b)
  ) : relations('c) =
  foldr2(
    (acc', n1_opt, n2_opt, t) => acc'->setRelation(t,
      f(n1_opt, n2_opt)
    ),
    empty,
    r1,
    r2
  );
