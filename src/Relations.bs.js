// Generated by BUCKLESCRIPT, PLEASE EDIT WITH CARE
'use strict';

var List = require("bs-platform/lib/js/list.js");
var Curry = require("bs-platform/lib/js/curry.js");

var order = {
  hd: /* ROW */0,
  tl: {
    hd: /* COL */1,
    tl: {
      hd: /* RIGHT */2,
      tl: {
        hd: /* SUB */4,
        tl: {
          hd: /* SUPER */3,
          tl: {
            hd: /* BELOW */6,
            tl: {
              hd: /* ABOVE */5,
              tl: {
                hd: /* CONTAINS */7,
                tl: {
                  hd: /* PRESUPER */8,
                  tl: /* [] */0
                }
              }
            }
          }
        }
      }
    }
  }
};

var empty = {
  right: undefined,
  super: undefined,
  sub: undefined,
  above: undefined,
  below: undefined,
  contains: undefined,
  preSuper: undefined,
  row: undefined,
  col: undefined
};

function toString(t) {
  switch (t) {
    case /* ROW */0 :
        return "row";
    case /* COL */1 :
        return "column";
    case /* RIGHT */2 :
        return "right";
    case /* SUPER */3 :
        return "super";
    case /* SUB */4 :
        return "sub";
    case /* ABOVE */5 :
        return "above";
    case /* BELOW */6 :
        return "below";
    case /* CONTAINS */7 :
        return "contains";
    case /* PRESUPER */8 :
        return "preSuper";
    
  }
}

function getRelation(r, t) {
  switch (t) {
    case /* ROW */0 :
        return r.row;
    case /* COL */1 :
        return r.col;
    case /* RIGHT */2 :
        return r.right;
    case /* SUPER */3 :
        return r.super;
    case /* SUB */4 :
        return r.sub;
    case /* ABOVE */5 :
        return r.above;
    case /* BELOW */6 :
        return r.below;
    case /* CONTAINS */7 :
        return r.contains;
    case /* PRESUPER */8 :
        return r.preSuper;
    
  }
}

function setRelation(r, t, v) {
  switch (t) {
    case /* ROW */0 :
        return {
                right: r.right,
                super: r.super,
                sub: r.sub,
                above: r.above,
                below: r.below,
                contains: r.contains,
                preSuper: r.preSuper,
                row: v,
                col: r.col
              };
    case /* COL */1 :
        return {
                right: r.right,
                super: r.super,
                sub: r.sub,
                above: r.above,
                below: r.below,
                contains: r.contains,
                preSuper: r.preSuper,
                row: r.row,
                col: v
              };
    case /* RIGHT */2 :
        return {
                right: v,
                super: r.super,
                sub: r.sub,
                above: r.above,
                below: r.below,
                contains: r.contains,
                preSuper: r.preSuper,
                row: r.row,
                col: r.col
              };
    case /* SUPER */3 :
        return {
                right: r.right,
                super: v,
                sub: r.sub,
                above: r.above,
                below: r.below,
                contains: r.contains,
                preSuper: r.preSuper,
                row: r.row,
                col: r.col
              };
    case /* SUB */4 :
        return {
                right: r.right,
                super: r.super,
                sub: v,
                above: r.above,
                below: r.below,
                contains: r.contains,
                preSuper: r.preSuper,
                row: r.row,
                col: r.col
              };
    case /* ABOVE */5 :
        return {
                right: r.right,
                super: r.super,
                sub: r.sub,
                above: v,
                below: r.below,
                contains: r.contains,
                preSuper: r.preSuper,
                row: r.row,
                col: r.col
              };
    case /* BELOW */6 :
        return {
                right: r.right,
                super: r.super,
                sub: r.sub,
                above: r.above,
                below: v,
                contains: r.contains,
                preSuper: r.preSuper,
                row: r.row,
                col: r.col
              };
    case /* CONTAINS */7 :
        return {
                right: r.right,
                super: r.super,
                sub: r.sub,
                above: r.above,
                below: r.below,
                contains: v,
                preSuper: r.preSuper,
                row: r.row,
                col: r.col
              };
    case /* PRESUPER */8 :
        return {
                right: r.right,
                super: r.super,
                sub: r.sub,
                above: r.above,
                below: r.below,
                contains: r.contains,
                preSuper: v,
                row: r.row,
                col: r.col
              };
    
  }
}

function _foldr(order, f, acc, r) {
  return List.fold_left((function (acc$prime, t) {
                return Curry._3(f, acc$prime, getRelation(r, t), t);
              }), acc, order);
}

function foldr(f, acc, r) {
  return _foldr(order, f, acc, r);
}

function foldr_rev(f, acc, r) {
  return _foldr(List.rev(order), f, acc, r);
}

function fold(f, acc, r) {
  return foldr((function (acc, opt, param) {
                return Curry._2(f, acc, opt);
              }), acc, r);
}

function mapr(f, r) {
  return foldr((function (r$prime, n_opt, t) {
                return setRelation(r$prime, t, Curry._2(f, n_opt, t));
              }), empty, r);
}

function map(f, r) {
  return mapr((function (n_opt, param) {
                return Curry._1(f, n_opt);
              }), r);
}

function toList(r) {
  return List.map((function (t) {
                return [
                        t,
                        getRelation(r, t)
                      ];
              }), order);
}

function findr_opt(f, r) {
  return List.find_opt((function (param) {
                return Curry._2(f, param[0], param[1]);
              }), toList(r));
}

function foldr2(f, acc, r1, r2) {
  return List.fold_left((function (acc$prime, t) {
                return Curry._4(f, acc$prime, getRelation(r1, t), getRelation(r2, t), t);
              }), acc, order);
}

function fold2(f, acc, r1, r2) {
  return foldr2((function (acc$prime, n1_opt, n2_opt, param) {
                return Curry._3(f, acc$prime, n1_opt, n2_opt);
              }), acc, r1, r2);
}

function map2(f, r1, r2) {
  return foldr2((function (acc$prime, n1_opt, n2_opt, t) {
                return setRelation(acc$prime, t, Curry._2(f, n1_opt, n2_opt));
              }), empty, r1, r2);
}

var types = {
  right: /* RIGHT */2,
  super: /* SUPER */3,
  sub: /* SUB */4,
  above: /* ABOVE */5,
  below: /* BELOW */6,
  contains: /* CONTAINS */7,
  preSuper: /* PRESUPER */8,
  row: /* ROW */0,
  col: /* COL */1
};

exports.types = types;
exports.order = order;
exports.empty = empty;
exports.toString = toString;
exports.getRelation = getRelation;
exports.setRelation = setRelation;
exports._foldr = _foldr;
exports.foldr = foldr;
exports.foldr_rev = foldr_rev;
exports.fold = fold;
exports.mapr = mapr;
exports.map = map;
exports.toList = toList;
exports.findr_opt = findr_opt;
exports.foldr2 = foldr2;
exports.fold2 = fold2;
exports.map2 = map2;
/* No side effect */
