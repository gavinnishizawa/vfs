// Generated by BUCKLESCRIPT, PLEASE EDIT WITH CARE
'use strict';

var Curry = require("bs-platform/lib/js/curry.js");
var Caml_option = require("bs-platform/lib/js/caml_option.js");

function ifSome(f, x) {
  if (x !== undefined) {
    return Caml_option.some(Curry._1(f, Caml_option.valFromOption(x)));
  }
  
}

function unwrap(o) {
  if (o !== undefined) {
    return Caml_option.valFromOption(o);
  }
  
}

function unsafe_some(opt) {
  if (opt !== undefined) {
    return Caml_option.valFromOption(opt);
  }
  throw {
        RE_EXN_ID: "Not_found",
        Error: new Error()
      };
}

exports.ifSome = ifSome;
exports.unwrap = unwrap;
exports.unsafe_some = unsafe_some;
/* No side effect */
