// Generated by BUCKLESCRIPT, PLEASE EDIT WITH CARE
'use strict';

var $$Array = require("bs-platform/lib/js/array.js");
var Curry = require("bs-platform/lib/js/curry.js");
var $$String = require("bs-platform/lib/js/string.js");
var Bbox$Vfs = require("./Bbox.bs.js");
var Node$Vfs = require("./Node.bs.js");
var Pervasives = require("bs-platform/lib/js/pervasives.js");
var Relations$Vfs = require("./Relations.bs.js");
var Selection$Vfs = require("./Selection.bs.js");

function makeInsert(node, r, context, subtree, internal) {
  if (!internal) {
    return [
            Node$Vfs.insert(node, r, subtree, context),
            []
          ];
  }
  var match = Node$Vfs.deleteFlagged(/* [] */0, Node$Vfs.insert(node, r, subtree, Node$Vfs.markForDeletion(subtree, context)));
  return [
          match[0],
          $$Array.of_list(match[1])
        ];
}

function makeInsertBefore(node, r, context, subtree, internal) {
  if (!internal) {
    return [
            Node$Vfs.insertBefore(node, r, subtree, context),
            []
          ];
  }
  var match = Node$Vfs.deleteFlagged(/* [] */0, Node$Vfs.insertBefore(node, r, subtree, Node$Vfs.markForDeletion(subtree, context)));
  return [
          match[0],
          $$Array.of_list(match[1])
        ];
}

function makeReplace(node, context, subtree, internal) {
  if (!internal) {
    return Node$Vfs.replaceSubtree(node, subtree, context);
  }
  var c = Node$Vfs.markForDeletion(subtree, context);
  var match = Node$Vfs.replaceSubtree(node, subtree, c);
  var primary_opt = match[0];
  var match$1 = primary_opt !== undefined ? Node$Vfs.deleteFlagged(/* [] */0, primary_opt) : [
      undefined,
      /* [] */0
    ];
  var floating = $$Array.of_list($$Array.fold_left((function (acc, param) {
              var f_floating = param[1];
              var f_opt = param[0];
              if (f_opt !== undefined) {
                return Pervasives.$at({
                            hd: f_opt,
                            tl: /* [] */0
                          }, Pervasives.$at(f_floating, acc));
              } else {
                return Pervasives.$at(f_floating, acc);
              }
            }), match$1[1], $$Array.map((function (fTree) {
                  return Node$Vfs.deleteFlagged(/* [] */0, fTree);
                }), match[1])));
  return [
          match$1[0],
          floating
        ];
}

function makeReplaceAll(node, context, subtree, internal) {
  if (!internal) {
    return Node$Vfs.replaceAll(node, subtree, context);
  }
  var c = Node$Vfs.markForDeletion(subtree, context);
  var match = Node$Vfs.replaceAll(node, subtree, c);
  var primary_opt = match[0];
  var match$1 = primary_opt !== undefined ? Node$Vfs.deleteFlagged(/* [] */0, primary_opt) : [
      undefined,
      /* [] */0
    ];
  var floating = $$Array.of_list($$Array.fold_left((function (acc, param) {
              var f_floating = param[1];
              var f_opt = param[0];
              if (f_opt !== undefined) {
                return Pervasives.$at({
                            hd: f_opt,
                            tl: /* [] */0
                          }, Pervasives.$at(f_floating, acc));
              } else {
                return Pervasives.$at(f_floating, acc);
              }
            }), match$1[1], $$Array.map((function (fTree) {
                  return Node$Vfs.deleteFlagged(/* [] */0, fTree);
                }), match[1])));
  return [
          match$1[0],
          floating
        ];
}

function makeGroup(primary, leftStr, rightStr, context, param) {
  return Node$Vfs.group(primary, leftStr === "" ? undefined : Node$Vfs.make("addGroup" + leftStr, leftStr), rightStr === "" ? undefined : Node$Vfs.make("addGroup" + rightStr, rightStr), context);
}

function getRelationLoc(r, box) {
  switch (r) {
    case /* COL */1 :
    case /* RIGHT */2 :
        return Bbox$Vfs.midRight(box);
    case /* SUPER */3 :
        return Bbox$Vfs.topRight(box);
    case /* SUB */4 :
        return Bbox$Vfs.botRight(box);
    case /* ABOVE */5 :
        return Bbox$Vfs.topCenter(box);
    case /* ROW */0 :
    case /* BELOW */6 :
        return Bbox$Vfs.botCenter(box);
    case /* CONTAINS */7 :
        return Bbox$Vfs.center(box);
    case /* PRESUPER */8 :
        return Bbox$Vfs.topLeft(box);
    
  }
}

function makeInsertOperation(node, r, context) {
  return {
          name: "Insert " + $$String.capitalize_ascii(Relations$Vfs.toString(r)),
          op: (function (param, param$1) {
              return makeInsert(node, r, context, param, param$1);
            }),
          nArgs: 1
        };
}

function makeInsertBeforeOperation(node, r, context) {
  return {
          name: "Insert " + Relations$Vfs.toString(r),
          op: (function (param, param$1) {
              return makeInsertBefore(node, r, context, param, param$1);
            }),
          nArgs: 1
        };
}

function makeInsertControl(node, r, getBox, context) {
  return {
          loc: getRelationLoc(r, Curry._1(getBox, node)),
          ops: [makeInsertOperation(node, r, context)]
        };
}

function makeGroupOperations(node, context, laterOps) {
  return {
          hd: {
            name: "Lift",
            op: (function (param, param$1) {
                var param$2 = Node$Vfs.deleteSubtree(node, context);
                return [
                        param$2[0],
                        $$Array.of_list({
                              hd: node,
                              tl: $$Array.to_list(param$2[1])
                            })
                      ];
              }),
            nArgs: 0
          },
          tl: {
            hd: {
              name: "Copy",
              op: (function (param, param$1) {
                  return [
                          context,
                          $$Array.of_list({
                                hd: node,
                                tl: /* [] */0
                              })
                        ];
                }),
              nArgs: 0
            },
            tl: laterOps
          }
        };
}

function makeNodeControls(node, getBox, context) {
  var insertControlOfRelation = function (r) {
    return makeInsertControl(node, r, getBox, context);
  };
  var match = Node$Vfs.tableNodeOf(node);
  var n = Node$Vfs.getNodeAt(match[0], context);
  var nodeInTable = n !== undefined ? n.nodeType === /* TABLE */3 : false;
  var partial_arg = Node$Vfs.getRoot(node);
  var partial_arg$1 = Node$Vfs.getRoot(node);
  var match$1 = node.nodeType;
  var match$2 = node.nodeType;
  return $$Array.of_list({
              hd: {
                loc: Bbox$Vfs.center(Curry._1(getBox, node)),
                ops: $$Array.of_list({
                      hd: {
                        name: "Replace",
                        op: (function (param, param$1) {
                            return makeReplace(partial_arg, context, param, param$1);
                          }),
                        nArgs: 1
                      },
                      tl: {
                        hd: {
                          name: "Replace All",
                          op: (function (param, param$1) {
                              return makeReplaceAll(partial_arg$1, context, param, param$1);
                            }),
                          nArgs: 1
                        },
                        tl: makeGroupOperations(node, context, {
                              hd: {
                                name: "Delete",
                                op: (function (param, param$1) {
                                    return Node$Vfs.deleteSubtree(Node$Vfs.getRoot(node), context);
                                  }),
                                nArgs: 0
                              },
                              tl: match$1 !== 2 ? /* [] */0 : ({
                                    hd: makeInsertOperation(node, /* CONTAINS */7, context),
                                    tl: /* [] */0
                                  })
                            })
                      }
                    })
              },
              tl: {
                hd: {
                  loc: Bbox$Vfs.midRight(Curry._1(getBox, node)),
                  ops: $$Array.of_list(Pervasives.$at({
                            hd: makeInsertOperation(node, /* RIGHT */2, context),
                            tl: /* [] */0
                          }, nodeInTable && false ? ({
                                hd: {
                                  name: "Insert Column Left",
                                  op: (function (param, param$1) {
                                      return makeInsertBefore(node, /* COL */1, context, param, param$1);
                                    }),
                                  nArgs: 1
                                },
                                tl: /* [] */0
                              }) : /* [] */0))
                },
                tl: {
                  hd: insertControlOfRelation(/* SUPER */3),
                  tl: {
                    hd: insertControlOfRelation(/* SUB */4),
                    tl: {
                      hd: {
                        loc: Bbox$Vfs.topCenter(Curry._1(getBox, node)),
                        ops: $$Array.of_list(Pervasives.$at({
                                  hd: makeInsertOperation(node, /* ABOVE */5, context),
                                  tl: /* [] */0
                                }, nodeInTable && false ? ({
                                      hd: {
                                        name: "Insert Row Above",
                                        op: (function (param, param$1) {
                                            return makeInsertBefore(node, /* ROW */0, context, param, param$1);
                                          }),
                                        nArgs: 1
                                      },
                                      tl: /* [] */0
                                    }) : /* [] */0))
                      },
                      tl: {
                        hd: {
                          loc: Bbox$Vfs.botCenter(Curry._1(getBox, node)),
                          ops: $$Array.of_list(Pervasives.$at({
                                    hd: makeInsertOperation(node, /* BELOW */6, context),
                                    tl: /* [] */0
                                  }, nodeInTable && false ? ({
                                        hd: makeInsertOperation(node, /* ROW */0, context),
                                        tl: /* [] */0
                                      }) : /* [] */0))
                        },
                        tl: {
                          hd: {
                            loc: Bbox$Vfs.midLeft(Curry._1(getBox, node)),
                            ops: $$Array.of_list(Pervasives.$at({
                                      hd: {
                                        name: "Insert Left",
                                        op: (function (param, param$1) {
                                            return makeInsertBefore(node, /* RIGHT */2, context, param, param$1);
                                          }),
                                        nArgs: 1
                                      },
                                      tl: /* [] */0
                                    }, nodeInTable && false ? ({
                                          hd: makeInsertBeforeOperation(node, /* COL */1, context),
                                          tl: /* [] */0
                                        }) : /* [] */0))
                          },
                          tl: match$2 !== 2 ? /* [] */0 : ({
                                hd: insertControlOfRelation(/* PRESUPER */8),
                                tl: /* [] */0
                              })
                        }
                      }
                    }
                  }
                }
              }
            });
}

function makeAllControls(getBox, context) {
  return $$Array.of_list(Node$Vfs.mapList((function (node) {
                    return makeNodeControls(Node$Vfs.getRoot(node), getBox, context);
                  }), context));
}

function makeSelectionControls(selection, getBox, context) {
  var selectionBox = Selection$Vfs.getSelectionBox(selection, getBox);
  var primary = selection[0];
  if (primary !== undefined) {
    if (Node$Vfs.size(primary) === 1 && selection[1].length === 0) {
      return makeNodeControls(primary, getBox, context);
    } else {
      return [{
                loc: Bbox$Vfs.center(selectionBox),
                ops: $$Array.of_list({
                      hd: {
                        name: "Replace",
                        op: (function (param, param$1) {
                            return makeReplace(primary, context, param, param$1);
                          }),
                        nArgs: 1
                      },
                      tl: {
                        hd: {
                          name: "Replace All",
                          op: (function (param, param$1) {
                              return makeReplaceAll(primary, context, param, param$1);
                            }),
                          nArgs: 1
                        },
                        tl: makeGroupOperations(primary, context, {
                              hd: {
                                name: "Delete",
                                op: (function (param, param$1) {
                                    return $$Array.fold_left((function (param, cur) {
                                                  var floating$prime = param[1];
                                                  var pri$prime_opt = param[0];
                                                  if (pri$prime_opt === undefined) {
                                                    return [
                                                            undefined,
                                                            floating$prime
                                                          ];
                                                  }
                                                  var param$1 = Node$Vfs.deleteSubtree(cur, pri$prime_opt);
                                                  return [
                                                          param$1[0],
                                                          $$Array.append(param$1[1], floating$prime)
                                                        ];
                                                }), Node$Vfs.deleteSubtree(primary, context), selection[1]);
                                  }),
                                nArgs: 0
                              },
                              tl: /* [] */0
                            })
                      }
                    })
              }];
    }
  } else {
    return [];
  }
}

function bool_of_opt(opt) {
  return opt !== undefined;
}

exports.makeInsert = makeInsert;
exports.makeInsertBefore = makeInsertBefore;
exports.makeReplace = makeReplace;
exports.makeReplaceAll = makeReplaceAll;
exports.makeGroup = makeGroup;
exports.getRelationLoc = getRelationLoc;
exports.makeInsertOperation = makeInsertOperation;
exports.makeInsertBeforeOperation = makeInsertBeforeOperation;
exports.makeInsertControl = makeInsertControl;
exports.makeGroupOperations = makeGroupOperations;
exports.makeNodeControls = makeNodeControls;
exports.makeAllControls = makeAllControls;
exports.makeSelectionControls = makeSelectionControls;
exports.bool_of_opt = bool_of_opt;
/* No side effect */
