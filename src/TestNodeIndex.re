
type indexedNode('a) = NodeIndex.indexedNode(Node.t(string, 'a));

let testIndex(tree, target) = TestUtil.test(
  () => NodeIndex.indexTree(tree) -> fst,
  target,
);

let testIndex0() = testIndex(
  Node.make("a", "a"),
  [|
    {
      i: 0,
      node: Node.make("a", "a"),
      relations: Relations.empty,
    }
  |],
);

let testIndex1() = testIndex(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  [|
    {
      i: 0,
      node: Node.make("a", "a")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
      relations: {
        ...Relations.empty,
        right: Some({
          i: 1,
          node: {
            ...Node.make("b", "b"),
            rPath: [Relations.RIGHT],
          },
          relations: Relations.empty,
        }),
      },
    },
    {
      i: 1,
      node: {
        ...Node.make("b", "b"),
        rPath: [Relations.RIGHT],
      },
      relations: Relations.empty,
    }
  |],
);

let testIndex2() = {
  let abc = Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b"))
  |> Node.relate(Relations.RIGHT, Node.make("c", "c"));
  let b = {
    ...Node.make("b", "b"),
    rPath: [Relations.SUPER],
  };
  let c = {
    ...Node.make("c", "c"),
    rPath: [Relations.RIGHT],
  };
  let iB : indexedNode('a) = {
    i: 1,
    node: b,
    relations: Relations.empty,
  };
  let iC : indexedNode('a) = {
    i: 2,
    node: c,
    relations: Relations.empty,
  };
  let iA : indexedNode('a) = {
    i: 0,
    node: abc,
    relations: {
      ...Relations.empty,
      super: Some(iB),
      right: Some(iC),
    },
  };

  testIndex(abc, [| iA, iB, iC |]);
};

let testIndex3() = {
  let abcde = Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c")))
  |> Node.relate(Relations.SUB, Node.make("d", "d"))
  |> Node.relate(Relations.RIGHT, Node.make("e", "e"));

  let b = {
    ...Node.make("b", "b"),
    rPath: [Relations.SUPER],
  }
  |> Node.relate(Relations.RIGHT, Node.make("c", "c"));
  let c = {
    ...Node.make("c", "c"),
    rPath: [Relations.RIGHT, Relations.SUPER],
  };
  let d = {
    ...Node.make("d", "d"),
    rPath: [Relations.SUB],
  };
  let e = {
    ...Node.make("e", "e"),
    rPath: [Relations.RIGHT],
  };
  let iE : indexedNode('a) = {
    i: 4,
    node: e,
    relations: Relations.empty,
  };
  let iD : indexedNode('a) = {
    i: 3,
    node: d,
    relations: Relations.empty,
  };
  let iC : indexedNode('a) = {
    i: 2,
    node: c,
    relations: Relations.empty,
  };
  let iB : indexedNode('a) = {
    i: 1,
    node: b,
    relations: {
      ...Relations.empty,
      right: Some(iC),
    },
  };
  let iA : indexedNode('a) = {
    i: 0,
    node: abcde,
    relations: {
      ...Relations.empty,
      super: Some(iB),
      sub: Some(iD),
      right: Some(iE),
    },
  };

  testIndex(abcde, [| iA, iB, iC, iD, iE |]);
};


let runIndex() = {
  [
    testIndex0,
    testIndex1,
    testIndex2,
    testIndex3,
  ]
  |> TestUtil.runTests("Index");
};


let run() = {
  runIndex();
};