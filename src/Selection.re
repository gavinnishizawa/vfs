
// open Util;
open Bbox;

type n('a, 'b) = Node.t('a, 'b);
type selection('a, 'b) = (
  option(n('a, 'b)),
  array(n('a,'b))
);

// 1. Selection
//   - Start and end corners => rectangle
let getRect(first: coord, second: coord) : bbox = {
  loc: {
    x: min(first.x, second.x),
    y: min(first.y, second.y),
  },
  size: {
    width: abs_float(second.x -. first.x),
    height: abs_float(second.y -. first.y),
  },
};

//   - Symbol bbox info -> nodes
//   - rect, nodes => nodes in rect
// Find nodes with bounding boxes that intersect with the given box
let getValuesInBox(box: bbox, getBbox: 'a => bbox, values: list('a)) : list('a) =
  values
  |> List.filter(v => v->getBbox->intersect(box));

let findLowestTieBreaker(
  rank: 'a => float,
  tieBreaker: ('a, 'a) => bool,
  list: list('a)
) : option('a) = {
  list
  |> List.fold_left(
    ((champ_o, score), cur) => {
      let curScore = cur->rank;
      switch(champ_o) {
        | None => (Some(cur), curScore)
        | Some(champ) => if (curScore < score
          || (curScore == score && tieBreaker(cur, champ))
          ) {
            (Some(cur), curScore)
          } else {
            (Some(champ), score)
          }
      }
    },
    (None, 0.)
  )
  // Return champ_o
  |> fst;
};

let findLowest(rank: 'a => float, list: list('a)) : option('a) =
  findLowestTieBreaker(rank, (_,_) => false, list);


//   - start, nodes in rect => closest node
let getClosest(start, getBbox: 'a => bbox, values: list('a)) : option('a) = {
  let dist(v) = v->getBbox |> distToBox(start);
  values |> findLowest(dist);
};

let getClosestSmallestFirst(start, getBbox: 'a => bbox, values: list('a)) : option('a) = {
  let dist(v) = v->getBbox |> distToBox(start);

  let firstIsSmaller(a, b) = a->getBbox->Bbox.area < b->getBbox->Bbox.area;

  values |> findLowestTieBreaker(dist, firstIsSmaller);
};


let getClosestNode(start, getBox, context) = getClosestSmallestFirst(
  start,
  getBox,
  context |> Node.mapList(n => n));

type selectionMisc('b) = {
  isSelected: bool,
  existingMisc: option('b),
};
type selectionNode('a, 'b) = Node.t(
  'a,
  selectionMisc('b),
);
type iSelectionNode('a, 'b) = NodeIndex.indexedNode(selectionNode('a, 'b));
type ufiSelectionNode('a, 'b) = UnionFind.tree(iSelectionNode('a, 'b));

let rec selectionTree(
  checkSelected: Node.t('a, 'b) => bool,
  context: Node.t('a, 'b)) : selectionNode('a, 'b) = {
  ...context,
  misc: Some({
    isSelected: context->checkSelected,
    existingMisc: context.misc,
  }),
  relations: context.relations
  |> Relations.map( n_opt => switch(n_opt) {
    | None => None
    | Some(n) => checkSelected->selectionTree(n)->Some
  }),
};

let isSelected(sNode: selectionNode('a, 'b)) : bool =
  switch(sNode.misc) {
    | None => false
    | Some(m) => m.isSelected
  };

let existingMisc(sNode: selectionNode('a, 'b)) : option('b) =
  switch(sNode.misc) {
    | None => None
    | Some(m) => switch(m.existingMisc) {
      | None => None
      | Some(em) => Some(em)
    }
  };

let rec nodeTree(sNode: selectionNode('a, 'b)) : Node.t('a, 'b) = {
  ...sNode,
  misc: sNode->existingMisc,
  relations: sNode.relations
  |> Relations.map( n_opt => switch(n_opt) {
    | None => None
    | Some(n) => n->nodeTree->Some
  }),
};

let getSelectionTree(box, getBox, context: Node.t('a, 'b)) = {
  let checkSelected(n) = n->getBox->Bbox.intersect(box);
  checkSelected->selectionTree(context)
};

let foldSelected(f, acc, tree) = tree
|> Node.fold(f, acc);

let getSelectedNodes(selectionContext) = selectionContext
  |> foldSelected(
    (acc, sn: selectionNode('a, 'b)) => sn->isSelected ? [sn->nodeTree, ...acc] : acc,
    []
  );

let getSelectionSubtrees(selectionContext) = {
  let (nodeIndex, iRoot) = selectionContext->NodeIndex.indexTree;

  let subtreeIndex = nodeIndex
  |> Array.map(
    iNode => UnionFind.makeNode(iNode),
  );

  let iRoot : iSelectionNode('a, 'b) = iRoot;

  iRoot
  |> NodeIndex.iterEdges(
    (_t, a: iSelectionNode('a, 'b), b: iSelectionNode('a, 'b)) =>
    if (a.node->isSelected && b.node->isSelected) {
      // Union b, a so that a will become parent when tied
      UnionFind.union(subtreeIndex[b.i], subtreeIndex[a.i]);
    }
  );

  // subtreeIndex
  // |> Array.iter((ufiNode : ufiSelectionNode('a)) => {
  //   Js.log3(ufiNode.value.node.node.value, ufiNode.value.node.isSelected, ufiNode.parent.value.node.node.value);
  // });

  let rec trimSelectedSubtree(
    root: ufiSelectionNode('a, 'b)
  ) : Node.t('a, 'b) = {
    let iNode = root.value;
    let sNode = iNode.node;
    {
      ...sNode,
      misc: sNode->existingMisc,
      relations: iNode.relations
      |> Relations.map(
        (iNode_opt: option(iSelectionNode('a, 'b))) => switch(iNode_opt) {
          | None => None
          | Some(iNode) => {
            let ufiNode = subtreeIndex[iNode.i];
            if ( UnionFind.find(ufiNode) === UnionFind.find(root)) {
              if (iNode.node->isSelected) {
                let trimmedUfiNode = trimSelectedSubtree(ufiNode);
                Some(trimmedUfiNode);
              } else {
                None
              }
            } else {
              None
            }
          }
        }
      ),
    };
  };

  let roots = subtreeIndex
  |> Array.map(UnionFind.find)
  |> Array.to_list
  |> List.filter((r : ufiSelectionNode('a, 'b)) => r.value.node->isSelected)
  |> Array.of_list;

  roots
  |> Array.sort(
    (a: ufiSelectionNode('a, 'b), b: ufiSelectionNode('a, 'b)) => a.value.i - b.value.i
  );

  // Js.log(roots
  // |> Array.map((root: ufiSelectionNode('a)) => root.value.i));

  let uniqueRoots = Array.fold_right(
    (cur, acc) => switch(acc) {
      | [] => [cur]
      | [hd, ..._] => (hd === cur) ? acc : [cur, ...acc];
    },
    roots,
    []
  );

  // Js.log(uniqueRoots
  // |> List.map((root: ufiSelectionNode('a)) => root.value.i)
  // |> Array.of_list
  // );

  let subtrees = uniqueRoots
  |> List.map(trimSelectedSubtree);

  // subtrees
  // |> List.iter(
  //   st => Node.string_of_node(v=>v, st)->Js.log
  // );

  subtrees;
};

let getSelection(pA, pB, maxClickDist, getBox, context) = {
  let box = getRect(pA, pB);
  // If distance is less than maxClickDist, consider it a click
  let isClick = Bbox.dist(pA, pB) < maxClickDist;
  let selectionContext = getSelectionTree(box, getBox, context);
  let selectedNodes = getSelectedNodes(selectionContext);
  let closestNode = isClick
    ? getClosestSmallestFirst(pA, getBox, selectedNodes)
    : getClosest(pA, getBox, selectedNodes);
  let subtrees = getSelectionSubtrees(selectionContext);

  switch(closestNode) {
    | None => ( None, subtrees );
    | Some(primary) => if (isClick) {
      (primary->Node.getRoot->Some, [])
    } else {
      let (primary, secondary) = subtrees
      |> List.partition(
        // partition out subtree with closest node from all subtrees
        subTree => switch(primary.id) {
          | None => false
          | Some(id) => subTree |> Node.hasId(id)
        }
      );

      switch(primary) {
        | [] => ( None , secondary )
        | [hd, ..._] => ( Some(hd), secondary )
      };
    }
  }
  // Convert secondary from list to array
  |> ((primary, secondary)) => (primary, secondary->Array.of_list);
};

let getSubtreeBoxes(subtree, getBox: n('a, 'b) => bbox) : array(bbox) =
  subtree |> Node.mapArray(getBox);

let getSelectionBox(selection : selection('a, 'b), getBox: n('a, 'b) => bbox) : bbox = {
  let (primary_opt, rest) = selection;
  let primaryBoxes = switch(primary_opt) {
    | None => [||]
    | Some(primary) => primary->getSubtreeBoxes(getBox);
  };


  rest
  |> Array.fold_left(
    (boxes, subtree) => subtree->getSubtreeBoxes(getBox)->Array.append(boxes),
    primaryBoxes)
  |> Bbox.boxesBox;
};