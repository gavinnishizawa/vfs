type id = Node.id;
open Bbox;

type rootedOperation('a, 'b) =
(Node.t('a, 'b), bool) => Node.opResult('a, 'b);
type namedOperation('a, 'b) = {
  name: string,
  op: rootedOperation('a, 'b),
  nArgs: int,
};

type control('a, 'b) = {
  loc: coord,
  ops: array(namedOperation('a, 'b)),
};


let makeInsert(node, r, context) : rootedOperation('a, 'b) =
  (subtree, internal) => if (internal) {
    let (primary_opt, floating) = context
    |> Node.markForDeletion(subtree)
    |> Node.insert(node, r, subtree)
    |> Node.deleteFlagged([]);

    (primary_opt, floating->Array.of_list);
  } else {
    (
      Some(Node.insert(node, r, subtree, context)),
      [||]
    );
  };

let makeInsertBefore(node, r, context) =
  (subtree, internal) => if (internal) {
    let (primary_opt, floating) = context
    |> Node.markForDeletion(subtree)
    |> Node.insertBefore(node, r, subtree)
    |> Node.deleteFlagged([]);

    (primary_opt, floating->Array.of_list);
  } else {
    (
      Some(Node.insertBefore(node, r, subtree, context)),
      [||]
    );
  };

let makeReplace(node, context) =
  (subtree, internal) => if (internal) {
    let (primary_opt, secondary) = context
    |> Node.markForDeletion(subtree)
    |> c => Some(c)
    |> Node.replaceSubtree(Some(node), Some(subtree));

    let (primary', pFloating) = switch(primary_opt) {
      | None => (None, [])
      | Some(primary) => primary|>Node.deleteFlagged([])
    };

    let floating = secondary
    |> Array.map(fTree => fTree|>Node.deleteFlagged([]))
    |> Array.fold_left((acc, (f_opt, f_floating)) => switch(f_opt) {
      | None => f_floating @ acc
      | Some(fTree) => [fTree] @ f_floating @ acc
    }, pFloating)
    |> Array.of_list;

    (primary', floating);
  } else {
    Node.replaceSubtree(Some(node), Some(subtree), Some(context));
  };

let makeReplaceAll(node, context) = 
  (subtree, internal) => if (internal) {
    let (primary_opt, secondary) = context
    |> Node.markForDeletion(subtree)
    |> c => Some(c)
    |> Node.replaceAll(Some(node), Some(subtree));

    let (primary', pFloating) = switch(primary_opt) {
      | None => (None, [])
      | Some(primary) => primary|>Node.deleteFlagged([])
    };

    let floating = secondary
    |> Array.map(fTree => fTree|>Node.deleteFlagged([]))
    |> Array.fold_left((acc, (f_opt, f_floating)) => switch(f_opt) {
      | None => f_floating @ acc
      | Some(fTree) => [fTree] @ f_floating @ acc
    }, pFloating)
    |> Array.of_list;

    (primary', floating);
  } else {
    Node.replaceAll(Some(node), Some(subtree), Some(context));
  };
  

let makeGroup(primary, leftStr, rightStr, context) =
  _ => Node.group(primary,
    leftStr == ""
      ? None
      : Some(Node.make("addGroup"++leftStr, leftStr)),
    rightStr == ""
      ? None
      : Some(Node.make("addGroup"++rightStr, rightStr)),
    context,
  );


let getRelationLoc(r, box) : coord = switch(r) {
  | Relations.RIGHT => box->midRight
  | Relations.SUPER => box->topRight
  | Relations.SUB => box->botRight
  | Relations.ABOVE => box->topCenter
  | Relations.BELOW => box->botCenter
  | Relations.CONTAINS => box->center
  | Relations.PRESUPER => box->topLeft
  | Relations.ROW => box->botCenter
  | Relations.COL => box->midRight
};

let makeInsertOperation(node, r, context) = {
  name: "Insert " ++ r->Relations.toString->String.capitalize_ascii,
  op: makeInsert(node, r, context),
  nArgs: 1,
};

let makeInsertBeforeOperation(node, r, context) = {
  name: "Insert " ++ r->Relations.toString,
  op: makeInsertBefore(node, r, context),
  nArgs: 1,
};


let makeInsertControl(node, r, getBox, context) = {
  loc: r->getRelationLoc(node->getBox),
  ops: [|
    node->makeInsertOperation(r, context)
  |]
};

// let makeNoOp(node, context) : rootedOperation('a) =
//   (subtree) => (
//     Some(context),
//     [||]
//   );

// let makeMatrixControls(node: Node.t('a), getBox, context) = {
//   if ( node.nodeType == Node.TABLE ) {

//     let cRef = ref([]);
//     let addControl(control, cRef) = {
//       cRef := [control, ...cRef^];
//       cRef;
//     };


//     for (i in 0 to node->Node.nRows) {
//       let rowBox = Node.nodesInRow(i, node)
//       |> List.map(getBox)
//       |> Array.of_list
//       |> Bbox.boxesBox;

//       // Create insert row controls
//       cRef
//       |> addControl(
//         {
//           loc: rowBox->topCenter,
//           ops: [|
//             {
//               name: "Insert Before Row " ++ i->string_of_int,
//               op: makeNoOp(node, context)
//             },
//           |]
//         }
//       )
//       |> addControl(
//         {
//           loc: rowBox->botCenter,
//           ops: [|
//             {
//               name: "Insert After Row " ++ i->string_of_int,
//               op: makeNoOp(node, context)
//             },
//           |]
//         }
//       )
//       |> ignore;

//       for (j in 0 to node->Node.nColumns) {
//         if (i == 0) { // only create once
//           let colBox = Node.nodesInColumn(j, node)
//           |> List.map(getBox)
//           |> Array.of_list
//           |> Bbox.boxesBox;

//           cRef
//           |> addControl(
//             {
//               loc: colBox->midLeft,
//               ops: [|
//                 {
//                   name: "Insert Before Column " ++ j->string_of_int,
//                   op: makeNoOp(node, context)
//                 },
//               |]
//             }
//           )
//           |> ignore;
//         };

//         switch(Node.tableCell(i, j, node)) {
//           | None => ()
//           | Some(cur) => cRef
//             |> addControl(
//               {
//                 loc: cur->getBox->center,
//                 ops: [|
//                   {
//                     name: "Replace cell ("
//                       ++ i->string_of_int ++ ", "
//                       ++ j->string_of_int ++ ")",
//                     op: makeNoOp(cur, context)
//                   },
//                 |]
//               }
//             ) |> ignore;
//         };
//       }
//     }
//     cRef^;
//   } else {
//     []
//   }
// };

let makeGroupOperations(node, context, laterOps) = [
  {
    name: "Lift",
    op: (_,_)=> {
      // delete subtree
      Node.deleteSubtree(node, context)
      |> ((f,s)) => (f,
        // put subtree in secondary result
        [node, ...s->Array.to_list]->Array.of_list)
    },
    nArgs: 0,
  },
  {
    name: "Copy",
    op: (_,_)=> {
      // put node in secondary result
      (Some(context), [node]->Array.of_list)
    },
    nArgs: 0,
  },
  // {
  //   name: "Square Root",
  //   op: _ => Node.makeRoot(node, context),
  //   nArgs: 0,
  // },
  // {
  //   name: "Group ()",
  //   op: makeGroup(node, "(", ")", context),
  //   nArgs: 0,
  // },
  // {
  //   name: "Group []",
  //   op: makeGroup(node, "[", "]", context),
  //   nArgs: 0,
  // },
  // {
  //   name: "Group {}",
  //   op: makeGroup(node, "\\{", "\\}", context),
  //   nArgs: 0,
  // },
  // {
  //   name: "Group {",
  //   op: makeGroup(node, "\\{", "", context),
  //   nArgs: 0,
  // },
  // {
  //   name: "Group without delmiters",
  //   op: makeGroup(node, "", "", context),
  //   nArgs: 0,
  // },
  ...laterOps,
];

let makeNodeControls(node: Node.t('a, 'b), getBox, context) = {
  let insertControlOfRelation(r) = node->makeInsertControl(r, getBox, context);
  let insertOperationOfRelation(r) = node->makeInsertOperation(r, context);
  let insertBeforeOperationOfRelation(r) = node->makeInsertBeforeOperation(r, context);

  let (tablePath, _) = Node.tableNodeOf(node);
  let nodeInTable = Node.getNodeAt(tablePath, context)
    |> fun
    | None => false
    | Some(n) => n.nodeType == Node.TABLE;

  [
    {
      loc: node->getBox->center,
      ops: [
        {
          name: "Replace",
          op: makeReplace(node->Node.getRoot, context),
          nArgs: 1,
        },
        {
          name: "Replace All",
          op: makeReplaceAll(node->Node.getRoot, context),
          nArgs: 1,
        },
        ...makeGroupOperations(node, context, [
          {
            name: "Delete",
            op: (_,_) => Node.deleteSubtree(node->Node.getRoot, context),
            nArgs: 0,
          },
          // Only add contains control for ROOT node types
          ...switch(node.nodeType) {
            | ROOT  => [ Relations.CONTAINS->insertOperationOfRelation ]
            | _ => []
          }
        ]),
      ] -> Array.of_list
    },
    {
      loc: node->getBox->midRight,
      ops: [ Relations.RIGHT->insertOperationOfRelation ]
        @ (nodeInTable && false // temp disable table ops
          ? [ {
              name: "Insert Column Left",
              op: node->makeInsertBefore(Relations.COL, context),
              nArgs: 1,
            } ]
          : []
        ) |> Array.of_list
    },
    Relations.SUPER->insertControlOfRelation,
    Relations.SUB->insertControlOfRelation,
    {
      loc: node->getBox->topCenter,
      ops: [ Relations.ABOVE->insertOperationOfRelation ]
        @ (nodeInTable && false // temp disable table ops
          ? [ {
              name: "Insert Row Above",
              op: node->makeInsertBefore(Relations.ROW, context),
              nArgs: 1,
            } ]
          : []
        ) |> Array.of_list
    },
    {
      loc: node->getBox->botCenter,
      ops: [ Relations.BELOW->insertOperationOfRelation ]
        @ (nodeInTable && false // temp disable table ops
          ? [ Relations.ROW->insertOperationOfRelation ]
          : []
        ) |> Array.of_list
    },
    {
      loc: node->getBox->midLeft,
      ops: [
        {
          name: "Insert Left",
          op: node->makeInsertBefore(Relations.RIGHT, context),
          nArgs: 1,
        },
        ] @ (nodeInTable && false // temp disable table ops
          ? [ Relations.COL->insertBeforeOperationOfRelation ]
          : []
        ) |> Array.of_list
    },
    ...switch(node.nodeType) {
      // Only add presuper control for ROOT node types
      | ROOT => [
        Relations.PRESUPER->insertControlOfRelation,
      ]
      | _ => []
    },
  ] |> Array.of_list;
};

// let mergeCloseControls(dist, controls) = {
//   controls |> Array.to_list
// }

let makeAllControls(getBox, context) = context
  |> Node.mapList(node => context
    |> makeNodeControls(node->Node.getRoot, getBox))
  |> Array.of_list;

let makeSelectionControls(selection, getBox, context) : array(control('a, 'b)) = {
  let selectionBox = selection->Selection.getSelectionBox(getBox);
  switch(selection->fst) {
    | None => [||]
    | Some(primary) => if (
        // single node selection and no secondary subtrees
        primary->Node.size == 1 && selection->snd->Array.length == 0
      )
        // use all node controls
        primary->makeNodeControls(getBox, context)
      else [|
        {
          loc: selectionBox->center,
          ops: [
            {
              name: "Replace",
              op: makeReplace(primary, context),
              nArgs: 1,
            },
            {
              name: "Replace All",
              op: makeReplaceAll(primary, context),
              nArgs: 1,
            },
            ...makeGroupOperations(primary, context, [
              {
                name: "Delete",
                op: (_,_) => selection->snd
                  // delete each selected secondary subtree
                  |> Array.fold_left(
                    ((pri'_opt, floating'), cur) => switch(pri'_opt) {
                      | None => (None, floating')
                      | Some(pri') => Node.deleteSubtree(cur, pri')
                        // merge all floating results together
                        |> ((f,s)) => (f, s->Array.append(floating'))
                    },
                    // delete primary selected subtree to start
                    Node.deleteSubtree(primary, context)
                  ),
                nArgs: 0,
              },
            ]),
          ] -> Array.of_list
        },
    |];

  };
};


// 4. Controls
//   - control points locations:
//     top left, top center, top right,
//     middle left, middle center, middle right,
//     bottom left, bottom center, bottom right
//   - control points operation: name, f
//   - control point: location, control point operations
//   - generate insert control points
//     + (middle left, insert before right)
//     + (middle right, insert after right)
//     + (top right, insert before super)
//     + (top center, insert before above)
//     + (bottom center, insert before below)
//     + (bottom right, insert before sub)
//     + (middle center, insert before in)
//   - generate replace control points
//     + (middle center, replace node)
//     + (middle center, replace all of this node)
//     + (middle center (group), replace subtree)
//     + (middle center (group), replace all of this subtree)
//     + (middle center (group), replace each in this subtree)
//   - generate delete control points
//     + (middle center, delete node)
//     + (middle center (group), delete subtree)
//   - generate matrix control points
//     + (middle left (matrix region), insert empty col before)
//     + (middle left (matrix region), insert col before with value here)
//     + (middle right (matrix region), insert empty col after)
//     + (middle right (matrix region), insert col after with value here)
//     + (top center (matrix region), insert empty row before)
//     + (top center (matrix region), insert row before with value here)
//     + (bottom center (matrix region), insert empty row after)
//     + (bottom center (matrix region), insert row after with value here)
//     + (middle center (matrix region), replace region)
//     + (middle center (matrix region), delete/empty region)
  let bool_of_opt(opt) = switch (opt) {
  | None => false
  | Some(_) => true
  };


  // type closestNode('a) = (coord, Tree.t('a)) => Tree.node('a);
  // type boxed('a) = ('a, bbox);
  // let closest(coord: coord, boxes: list(boxed('a))) : option('a) = {
  //   None;
  // };
  // let boxControl(control) : boxed(control('a)) = {
  //   (control, pBox(control.loc));
  // };
  // let closestControl(c : coord, node: id, t: Tree.t('a)) : option(control('a)) = {
  //   let controls = getControls(node, t);
  //   let cBoxes : list(boxed(control('a))) = controls
  //   |> List.map(boxControl);
  //   let cc = closest(c, cBoxes);
  //   cc;
  //   // |> closest(c);
  // };