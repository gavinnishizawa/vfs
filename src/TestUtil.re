
let toJSON(v: 'a) = v
  -> Js.Json.stringifyAny
  -> (fun
    | None => ""
    | Some(s) => s
  )
  -> Js.Json.parseExn
  -> Js.Json.stringifyWithSpace(2);

let rec repeatPipe(f, i, t) = {
  switch(i > 0) {
    | true => repeatPipe(f, i - 1, t |> f)
    | false => t
  };
};

let timePipe(name, f, t) = {
  Js.Console.timeStart(name);
  let t' = f(t);
  Js.Console.timeEnd(name);
  t';
};

let testEq(f, eq, target) = {
  let result = f();
  if (result->eq(target)) {
    true;
  } else {
    Js.Console.error4(
    // Js.log4(
      "! Test failed. \n  Expected:\t",
      target -> Js.Json.stringifyAny,
      "\n  but got:\t",
      result -> Js.Json.stringifyAny,
    );
    false;
  }
};
let test(f, target) = testEq(f, (a,b) => a == b, target);

let runTests(title, tests) = {
  let total = tests->List.length
  Js.log2("\n" ++ title, " Tests:");
  Js.log3("Running ", total, " tests.");

  tests
  |> List.mapi((i,t) => {
    let r = t();
    Js.log4("   Test ", i, ":", r ? "PASS" : "FAIL");
    r;
  })
  |> List.partition(r => r)
  |> ((pass, fail)) => Js.log4(
    "Summary:\n   Passing: ", pass->List.length,
    "\n   Failing: ", fail->List.length
  );
};
