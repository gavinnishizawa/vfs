let Test = require("./src/Test.bs.js");
let TestJs = require("./src/TestJs.js");

function run(name, f) {
  console.log(`${name} :: start`)
  console.log("-------------")
  f();
  console.log("-------------")
  console.log(`${name} :: end\n\n`)
};

run("Test", Test.run);
// run("TestJs", TestJs.run);